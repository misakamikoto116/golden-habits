<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAyyamulBidhsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ayyamul_bidh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('date_hijriyah');
            $table->integer('month_hijriyah');
            $table->integer('year_hijriyah');
            $table->date('date_masehi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ayyamul_bidh');
    }
}
