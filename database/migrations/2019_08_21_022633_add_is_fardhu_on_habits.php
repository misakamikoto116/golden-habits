<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsFardhuOnHabits extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('habits', function (Blueprint $table) {
            $table->tinyInteger('isFardhu')->default(0)->after('sub_main_habits_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
