<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomefieldsFromHabits extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('habits', function (Blueprint $table) {
            $table->unsignedBigInteger('main_habits_id')->nullable()->after('periode_trigger');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->unsignedBigInteger('sub_main_habits_id')->nullable()->change();
        });

        Schema::table('main_habits', function (Blueprint $table) {
            $table->tinyInteger('is_have_sub_main_habits')->default(1)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
