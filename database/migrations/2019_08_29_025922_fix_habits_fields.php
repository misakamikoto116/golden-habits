<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixHabitsFields extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('main_habits', function (Blueprint $table) {
            $table->text('name')->change();
        });

        Schema::table('sub_main_habits', function (Blueprint $table) {
            $table->text('name')->change();
        });

        Schema::table('habits', function (Blueprint $table) {
            $table->text('name')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
