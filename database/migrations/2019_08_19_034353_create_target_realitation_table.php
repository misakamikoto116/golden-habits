<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetRealitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_realitation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('target');
            $table->unsignedBigInteger('main_habits_id');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->string('priod');

            $table->text('result_target')->nullable();

            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_realitation');
    }
}
