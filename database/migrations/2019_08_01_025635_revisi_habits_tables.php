<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevisiHabitsTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('main_habits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('sub_main_habits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('main_habits_id');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->timestamps();
        });

        Schema::create('habits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->tinyInteger('type');
            $table->string('periode_trigger');
            $table->unsignedBigInteger('sub_main_habits_id');
            $table->foreign('sub_main_habits_id')->references('id')->on('sub_main_habits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
