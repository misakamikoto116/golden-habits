<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreSubMainHabitsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('score_sub_main_habits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('score');
            $table->unsignedBigInteger('sub_main_habits_id');
            $table->foreign('sub_main_habits_id')->references('id')->on('sub_main_habits');
            $table->dateTime('date');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('score_sub_main_habits');
    }
}
