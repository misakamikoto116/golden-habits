<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsHatamFromRealitationTadarus extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('realitation_tadarus', function (Blueprint $table) {
            $table->tinyInteger('isHatam')->after('ayat')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('realitation_tadarus', function (Blueprint $table) {
        });
    }
}
