<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealitationZisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realitation_zis', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->double('nominal_income');
            $table->double('nominal_realization');
            $table->double('percentage_realization');
            $table->unsignedBigInteger('main_habits_id');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamp('date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realitation_zis');
    }
}
