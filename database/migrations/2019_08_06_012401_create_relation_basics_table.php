<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationBasicsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('relation_basics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('value')->default(0);
            $table->unsignedBigInteger('habits_id')->nullable();
            $table->foreign('habits_id')->references('id')->on('habits');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->datetime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('relation_basics');
    }
}
