<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealitationReadingsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('realitation_readings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('duration')->nullable();
            $table->integer('juz')->nullable();
            $table->tinyInteger('is_read_quran')->default(0);
            $table->datetime('date');
            $table->string('place');
            $table->string('surah')->nullable();
            $table->integer('ayat')->nullable();
            $table->integer('page_book')->nullable();
            $table->string('title_book')->nullable();
            $table->unsignedBigInteger('main_habits_id');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('realitation_readings');
    }
}
