<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOnTargetRealitation extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('target_realitation', function (Blueprint $table) {
            $table->integer('jumlah_khatam')->nullable()->after('priod');
            $table->integer('bulan')->nullable()->after('jumlah_khatam');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
