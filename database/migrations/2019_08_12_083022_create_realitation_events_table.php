<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealitationEventsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('realitation_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('place')->nullable();
            $table->string('key_speaker')->nullable();
            $table->string('theme');
            $table->tinyInteger('is_organization')->default(0);
            $table->string('organization_name')->nullable();
            $table->string('start_time');
            $table->string('end_time');
            $table->string('duration')->nullable();
            $table->unsignedBigInteger('main_habits_id');
            $table->foreign('main_habits_id')->references('id')->on('main_habits');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('realitation_events');
    }
}
