<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCodeFromInstitutions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('institutions', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('institutions', function (Blueprint $table) {
        });
    }
}
