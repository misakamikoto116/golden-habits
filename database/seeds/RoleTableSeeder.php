<?php

use Illuminate\Database\Seeder;
/* Models */
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
            ], [
                'name' => 'santri',
            ], [
                'name' => 'user admin',
            ], [
                'name' => 'admin instansi',
            ],
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate($role);
        }
    }
}
