<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call(InstituteSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(HabitsSeeder::class);
    }
}
