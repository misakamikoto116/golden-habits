<?php

use App\Models\Group;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use App\Models\Institution;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $roles = Role::pluck('name');

        if ($roles->isNotEmpty()) {
            $checkUser = User::get();
            $institute = Institution::get();
            $grup = Group::get();
            if ($checkUser->count() <= 0) {
                for ($i = 0; $i < count($roles); ++$i) {
                    $user = User::create([
                        'name' => $roles[$i],
                        'email' => str_replace(' ', '_', $roles[$i]).'@gmail.com',
                        'password' => bcrypt('123123'),
                        'gender' => 'Laki-laki',
                        'phone' => '081233454566',
                        'group_id' => $grup->random()->id,
                    ]);
                    $user->assignRole($roles[$i]);
                }
            }
        }
    }
}
