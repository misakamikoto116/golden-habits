<?php

use App\Models\Habits;
use Illuminate\Database\Seeder;
use App\Models\MainHabits;
use App\Models\SubMainHabits;

class HabitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $checkHabits = MainHabits::get();
        if ($checkHabits->isEmpty()) {
            $this->insertHabits();
        }
    }

    public function insertHabits()
    {
        $habits = [
            'Tertib Shalat' => [
                'Maghrib' => [
                    'Shalat Tahiyatul Masjid jelang Shalat Maghrib',
                    'Shalat Rawatib Qabliyah Maghrib',
                    'Shalat Maghrib',
                    'Shalat Maghrib di Awal Waktu',
                    'Shalat Maghrib Berjama’ah',
                    'Shalat Maghrib di Masjid',
                    'Berdoa dan berdzikir bakda shalat Maghrib',
                    'Shalat Rawatib Bakdiyah Maghrib',
                ],
                'Isya' => [
                    'Shalat Tahiyatul Masjid Jelang Isya atau tetap di masjid sejak maghrib',
                    'Shalat Isya',
                    'Shalat Isya di Awal Waktu',
                    'Shalat Isya berjamaah',
                    'Shalat Isya di Masjid',
                    'Berdoa dan berdzikir setelah s halat Isya',
                    'Shalat Rawatib Bakdiyah Isya',
                ],
                'Subuh' => [
                    'Shalat Tahiyatul Masjid jelang Shalat Subuh',
                    'Shalat Qabliyah Subuh',
                    'Shalat Subuh',
                    'Shalat Subuh di Awal Waktu',
                    'Shalat Subuh  berjamaah',
                    'Shalat Subuh di Masjid',
                    'Berdoa dan berdzikir setelah shalat subuh',
                    "I'tikaf Subuh - Syuruq",
                ],
                'Dhuhur' => [
                    'Shalat Tahiyatul Masjid sebelum Shalat Dhuhur',
                    'Shalat Rawatib Qabliyah Dhuhur',
                    'Shalat Dhuhur',
                    'Shalat Dhuhur di Awal Waktu',
                    'Shalat Dhuhur berjamaah',
                    'Shalat Dhuhur di Masjid',
                    'Berdoa dan berdzikir setelah Shalat Dhuhur',
                    'Shalat Rawatib Bakdiyah Dhuhur',
                ],
                'Ashar' => [
                    'Shalat Tahiyatul Masjid sebelum Shalat Ashar',
                    'Shalat Rawatib Qabliyah Ashar',
                    'Shalat Ashar',
                    'Shalat Ashar di Awal Waktu',
                    'Shalat Ashar Berjamaah',
                ],
                'Tahajud' => [
                    'Shalat Tahajud',
                    'Shalat Tahajud pada 1/3 malam akhir',
                    "Shalat Tahajud hingga 7 Raka'at",
                    'Shalat Tahajud lengkap 11 Raka’at',
                ],
                'Dhuha' => [
                    'Shalat Syuruq/Dhuha',
                    'Shalat Dhuha  hingga 4 raka’at',
                    'Shalat Dhuha hingga 8 raka’at',
                ],
            ],
            'Tertib Puasa' => [
                'Ramadhan' => ['Muncul setiap bulan Ramadhan'],
                'Ayyamu Bidh' => ['Muncul Setiap tgl 13, 14, 15 bulan hijryah'],
                'Senin - Kamis' => ['Muncul Setiap hari Senin dan Kamis'],
                'Daud' => ['Muncul Selang-Seling'],
            ],
            'Tertib Zakat - Infak - Sedekah (ZIS)' => [
                'Pendapatan' => ['Sumber (dari Pemberi Kerja/Usaha)'],
                'Norma ZIS Dipilih' => ['Dalam %, minimal 2,5%'],
                'Nilai ZIS' => ['Norma yang dipilih dikalikan pendapatan'],
                'ZIS dibayar' => ['Nominal yang dibayarkan'],
            ],
            'Tertib Beradab Islami' => [
                'Terhadap Diri Sendiri' => [
                    'Mengawali aktifitas selalu dengan basmalah',
                    'Mengakhiri aktifitas selalu dengan hamdalah',
                    'Makan – Minum selalu dengan tangan kanan, secukupnya, tidak mencela, dan menghabiskan yang diambil',
                    'Memberi dan menerima sesuatu selalu dengan tangan kanan',
                    'Beristinjak - memegang  kemaluan selalu dengan tangan kiri',
                    'Berpakaian menutup aurat dan rapi ketika ada orang lain yang bukan mahram, memakainya dari sebelah kanan dan melepasnya dari sebelah kiri',
                    'Bersin selalu membaca hamdalah',
                    'Tidur selalu memiringkan badan ke kanan, berdoa sebelumnya dan saat bangun',
                    'Pergi selalu berpamitan, bertawakkal, dan berdo’a',
                    'Menjaga hak tubuh (istirahat secukupnya, kebersihan, kesehatan)',
                ],
                'Terhadap Orang Lain' => [
                    'Selalu memberikan senyum, salam dan sapa saat berjumpa dengan orang tua, saudara, teman, tetangga, relasi, customer',
                    'Selalu menjawab salam orang lain',
                    'Menjaga pembicaraan selalu baik',
                    'Menjaga bahasa tubuh selalu baik',
                    'Tidak berbohong',
                    'Tidak berbicara buruk (ghibah – mencela – mengumpat – mencaci)',
                    'Menghormati yang lebih tua',
                    'Menyayangi yang lebih muda',
                    'Mendoakan orang bersin yang, membaca alhamdulillah dengan yarhamukallah',
                    'Tidak mengambil hak orang lain tanpa ijin',
                ],
                'Terhadap Tempat/Alat' => [
                    'Masuk rumah selalu dengan salam dan langkah kaki kanan, keluar rumah bertawakkal dan langkah kaki kiri',
                    'Masuk toilet selalu berdoa dan langkah kaki kiri, keluar berdoa dan langkah kaki kanan',
                    'Masuk selalu masjid berdoa dan langkah kaki kanan, keluar masjid berdoa dan langkah kaki kiri',
                    'Melaksanakan shalat tahiyatul masjid saat masuk sebelum duduk',
                    'Selalu berdoa saat naik kendaraan dan sampai tujuan',
                    'Masuk tempat kerja/belajar selalu berdoa dan langkah kaki kanan, keluar bertawakkal dan langkah kaki kiri',
                    'Di jalan selalu bertawakkal, tidak membahayakan orang lain, dan menjaga hak pengguna jalan lainnya',
                    'Selalu menjaga kebersihan dan kerapihan rumah yang ditempati, masjid, tempat kerja/belajar, dan tempat-tempat lain yang didatangi',
                    'Merawat dan menjaga alat kerja/belajar dalam keadaan siap pakai, bersih, dan rapi',
                    'Membuang sampah pada tempatnya',
                ],
            ],
            'Tertib Tadarrus Al-Quran' => [
                null,
            ],
            'Tertib Membaca' => [
                'Membaca' => [
                    'Saya membaca tafsir al-Qur’an hari ini',
                    'Saya membaca Hadits hari ini',
                    'Saya membaca buku tentang Islam hari ini',
                    'Saya membaca buku tentang ilmu yang saya tekuni hari ini',
                    'Saya membaca lebih dari 60 menit hari ini',
                ],
            ],
            'Tertib Mengikuti Pengajian' => [
                'Ngaji' => [
                    'Saya mengikuti  pengajian/ta’lim hari ini',
                    'Saya mencatat dan membuat resume isi pengajian',
                    'Saya membuat rencana tindak lanjut atas materi pengajian',
                    'Pengajian yang saya ikuti dilaksanakan di mesjid"',
                    'Saya berkomitmen melaksanakan rencana tindak lanjut pengajian',
                ],
            ],
            'Tertib Berdakwah - Berorganisasi' => [
                'Berorganisasi' => [
                    'Saya mengajak orang lain shalat di awal waktu – berjamaah  – di masjid',
                    'Saya mengajak orang lain beramal shaleh sesuai ajaran Islam',
                    'Saya mencegah orang lain berbuat buruk/maksiat',
                    'Saya menyampaikan tentang  al-Qur’an/Al Hadits kepada orang lain (perseorangan)',
                    'Saya menyampaikan pengajian/ceramah kepada sekelompok orang (jama’ah)',
                    'Saya ikut menyelenggarakan pengajian/majlis ilmu',
                    'Saya mengikuti kegiatan masjid di luar shalat',
                    'Saya mengikuti rapat organisasi',
                    'Saya melaksanakan tugas-tugas organisasi yang menjadi tanggungjawab saya',
                    'Saya ikut menyukseskan kegiatan orgaisasi dengan dana, tenaga, atau pikiran',
                ],
            ],
            'Berpikir Positif' => [
                'Berpikir Positif' => [
                    'Saya tidak membicarakan keburukan keluarga, saudara, guru, pemimpin, anak buah, atau teman  saya dengan orang lain',
                    'Saya memaafkan perbuatan keluarga, saudara, guru, pemimpin, anak buah, atau teman saya yang tidak menyenangkan',
                    'Saya mensyukuri kesehatan yang masih bisa saya nikmati hari ini, dan mengucapkan: Alhamdulillah, terima kasih ya Allah atas anugerah kesehatan yang Engkau berikan hari ini.',
                    'Saya mensyukuri riziki saya hari ini dengan mengucapkan: Alhamdulillah, terima kasih ya Allah atas anugerah rezeki yang cukup, mohon Engkau berkenan menambahnya agar bisa lebih banyak beramal shaleh',
                    'Saya mensyukuri amal shaleh saya hari ini dengan mengucapkan: Alhamdulillah, terima kasih ya Allah Engkau bimbing aku beramal shaleh dan terhindar dari amal maksiat',
                    'Saya mensyukuri capaian saya hari ini dan menggucapkan: Alhamdulillah, terima kasih ya Allah atas bimbingan-Mu untuk menyelesaikan tugas-tugasku dan mencapai targetku hari ini menjadi lebih baik.',
                    'Saya bersabar atas masalah kesehatan yang saya alami hari ini, bertawakkal kepada Allah, memohon kepada-Nya agar mengampuni dosa-dosa saya dan menganugerahi kesehatan yang lebih baik',
                    'Saya bersabar atas kesulitan yang saya alami hari ini, tidak mengeluh, mengucapkan hasbiyallah ni’mal maula wa ni’mal wakil/nashir dan bersiap menerima anugerah kemudahan dari Allah.',
                    'Saya bersabar atas kejadian yang tidak menyenangkan yang saya alami hari ini, dan memohon Allah menganugerahi ketangguhan.',
                    'Saya bersabar atas peristiwa alam (hujan, panas, banjir, dingin, badai, dll) yang saya alami, tidak menjadikannya sebagai alasan untuk tidak menunaikan tugas/ janji saya',
                ],
            ],
        ];

        $model_main_habits = [];
        $model_sub_main_habits = [];
        $model_habits = [];
        $indexMain = 1;
        $indexSub = 1;
        foreach ($habits as $mainHabits => $item) {
            if ($item) {
                $model_main_habits[] = ['name' => $mainHabits];
                foreach ($item as $subMain => $habits) {
                    $model_sub_main_habits[] = [
                        'main_habits_id' => $indexMain,
                        'name' => $subMain,
                    ];
                    if ($habits) {
                        foreach ($habits as $habit) {
                            $model_habits[] = [
                                'name' => $habit,
                                'sub_main_habits_id' => $indexSub,
                                'type' => rand(0, 1),
                                'periode_trigger' => '-',
                                'isFardhu' => rand(0, 1),
                            ];
                        }
                    }
                    ++$indexSub;
                }
            }
            ++$indexMain;
        }

        MainHabits::insert($model_main_habits);
        SubMainHabits::insert($model_sub_main_habits);
        Habits::insert($model_habits);
    }
}
