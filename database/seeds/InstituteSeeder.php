<?php

use Illuminate\Database\Seeder;
use App\Models\Institution;
use App\Models\Group;

class InstituteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $cek = Institution::get();
        if ($cek->count() < 1) {
            $institution_data[0] = [
                'name' => 'Qodr',
                'address' => 'Jogjakarta',
            ];

            $institution_data[1] = [
                'name' => 'Pesantren Mihajussunah Samarinda',
                'address' => 'Samarinda',
            ];

            $institution_data[2] = [
                'name' => 'Darut Thoyyibah',
                'address' => 'Samarinda',
            ];

            Institution::insert($institution_data);
            $institution = Institution::get();
            foreach ($institution as $int => $item) {
                $group[$int] = [
                    'name' => 'Group '.($int + 1),
                    'group_code' => 'GRP/00'.($int + 1),
                    'institution_id' => $item->id,
                ];
            }
            Group::insert($group);
        }
    }
}
