<?php

namespace App\Repositories;

use App\Models\Group;
use Generator\Interfaces\RepositoryInterface;
use App\Models\Institution;
use App\Models\User;
use Auth;
use Exception;
class GroupInstitutionRepository implements RepositoryInterface
{
    public function __construct(
        Group $model,
        User $user
    ) {
        $this->model = $model;
        $this->user = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->where('institution_id', Auth::user()->institution_id)
                    ->filter($request)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        if($model->user()->count() > 0){
            throw new Exception("Error, group sudah memiliki user", 401);
        }

        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $data['institution_id'] = Auth::user()->institution_id;
        if($data['institution_id']  === null){
            throw new Exception("Anda belum memiliki institusi/lembaga", 401);
        }
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['institution_id'] = Auth::user()->institution_id;
        if($data['institution_id'] === null){
            throw new Exception("Anda belum memiliki institusi/lembaga", 401);
        }
        $model = $this->model->create($data);
        if(isset($data['user_name']) && isset($data['user_email']) && isset($data['user_password'])){
            $user = $this->user->create([
                'name' => $data['user_name'],
                'email' => $data['user_email'],
                'password' => bcrypt($data['user_password']),
                'group_id' => $model->id,
                'role_id' => 4
            ]);
            $user->assignRole(4); //admin instansi
        }
        
        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
