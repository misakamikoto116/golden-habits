<?php

namespace App\Repositories;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

trait TraitUploadImage
{
    public function photoUploaded($request, $dir, $pastData = null)
    {
        if (isset($request['image'])) {
            if (isset($pastData)) {
                $this->deletePhoto($dir, $pastData);
            }

            $file_profile = $request['image'];

            if (empty($file_profile)) {
                return null;
            }
            $profile = time().str_slug($file_profile->getClientOriginalName(), '_').'.'.$file_profile->getClientOriginalExtension(); /* PART: generate file name */

            $this->SavePhoto($file_profile, public_path('storage/'.$dir.'/'), 600); /* Save File Trough */

            return $profile;
        }

        return $pastData;
    }

    public function SavePhoto($image, $path, $resize)
    {
        $file = $image;
        $photo = time().str_slug($file->getClientOriginalName(), '_').'.'.$file->getClientOriginalExtension();
        $file->move($path, $photo);
        $img = Image::make($path.$photo);
        $img->resize($resize, $resize, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }

    public function deletePhoto($dir, $pastData)
    {
        return File::delete(public_path('storage/'.$dir.'/'.$pastData));
    }
}
