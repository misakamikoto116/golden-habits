<?php

namespace App\Repositories;

use App\Models\AyyamulBidh;
use Generator\Interfaces\RepositoryInterface;

class AyyamulBidhRepository implements RepositoryInterface
{
    public function __construct(
        AyyamulBidh $model
    ) {
        $this->model = $model;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $parent = $model->sub_main_habits_id;
        $model->delete();

        return strval($parent);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $data = $this->convertDate($data);
        $model->update($data);

        return $data;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data = $this->convertDate($data);
        $this->model->create($data);

        return $data;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }

    public function convertDate($data): array
    {
        $data['date_masehi'] = date('Y-m-d', strtotime($data['date_masehi']));

        return $data;
    }
}
