<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Exception;

class RoleRepository implements RepositoryInterface
{
    public function __construct(
        Role $model,
        User $user
    ) {
        $this->model = $model;
        $this->user = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->withCount('users')
                    ->filter($request)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $user = $this->user->role($model->name)->get();

        /* Tidak bisa menghapus jika role memiliki user */
        if ($user->isNotEmpty()) {
            throw new Exception('Tidak dapat menghapus roles yang memiliki user', 401);
        }
        $model->delete();

        return strval($model);
    }

    private function setSessionFlash($message, $level)
    {
        Session::flash('flash_notification', [
            'message' => $message,
            'level' => $level,
        ]);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $model = $this->model->create($data);

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
