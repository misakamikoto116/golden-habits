<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\SubMainHabits;
use App\Models\Habits;
use App\Models\MainHabits;

class HabitsRepository implements RepositoryInterface
{
    public function __construct(
        Habits $model,
        SubMainHabits $sub_main_habits,
        MainHabits $main_habits
    ) {
        $this->model = $model;
        $this->sub_main_habits = $sub_main_habits;
        $this->main_habits = $main_habits;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('id', 'asc')
                    ->paginate(10);
    }

    public function getItemByParent($request)
    {
        $parent = $request->get('parent');
        if ($request->get('is_main_habits') === 'true') {
            $data = $this->model->where('main_habits_id', $parent);
            $parent_record = $this->main_habits->findOrFail($parent);
        } else {
            $data = $this->model->where('sub_main_habits_id', $parent);
            $parent_record = $this->sub_main_habits->findOrFail($parent);
        }
        $data = $data->filter($request)
        ->orderBy('id', 'asc')
        ->get();

        $response = [
            'detail' => $data,
            'parent' => $parent_record,
        ];

        return $response;
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $parent = $model->sub_main_habits_id;
        $model->delete();

        return strval($parent);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $data['parent_id'] = $model->sub_main_habits_id;
        $data['isFardhu']  = empty($data['isFardhu']) ? 0 : 1;

        if ($model->main_habits_id != null) {
            $data['is_main_habits'] = 'true';
            $data['parent_id'] = $model->main_habits_id;
        }
        $model->update($data);

        return $data;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['isFardhu']  = empty($data['isFardhu']) ? 0 : 1;
        if ($data['is_main_habits'] === 'true') {
            $this->main_habits->findOrFail($data['parent_id']);
            $data['main_habits_id'] = $data['parent_id'];
        } else {
            $this->sub_main_habits->findOrFail($data['parent_id']);
            $data['sub_main_habits_id'] = $data['parent_id'];
        }
        $this->model->create($data);

        return $data;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
