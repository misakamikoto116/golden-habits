<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\Notification;
use App\Models\Institution;
use App\Models\Group;
use App\Models\User;
use Auth;

class NotifcationRepository implements RepositoryInterface
{
    public function __construct(
        Notification $model
    ) {
        $this->model = $model;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);

        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if ($data['group'] == 'all') {
            $groups = array_keys($this->getGroup());
            $token  = User::where(function ($query) use ($groups) {
                foreach ($groups as $group) {
                    $query->orWhere('group_id', $group);
                }
            })->whereNotNull('fcm_token')->pluck('fcm_token');
        } else {
            $token = User::where('group_id', $data['group'])->whereNotNull('fcm_token')->pluck('fcm_token');
        }
        
        $curl = curl_init();
        
        $body = [
            "registration_ids" => $token,
            "data" => [
                "title" => $data['title'],
                "body"  => $data['message']
            ]
        ];
        
        $header = [
            "Authorization : key=AIzaSyC4nC3wjB1SrkyU0vydj3YPZqGiOs_HTdM",
            "Content-type: application/json"
        ];

        curl_setopt($curl, CURLOPT_URL,"https://fcm.googleapis.com/fcm/send");
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curlResponse = json_decode(curl_exec($curl));

        curl_close ($curl);

        $data['isSuccess'] = (!is_null($curlResponse)) ? $curlResponse->success : 0;
        $data['date']      = date('Y-m-d H:i:s');
        $model = $this->model->create($data);

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }

    public function getGroup()
    {
        $institutionUser = Auth::user()->institution_id;

        if (!is_null($institutionUser)) {
            $group = Institution::find($institutionUser)->group->pluck('name', 'id');
        } else {
            $group = Group::pluck('name', 'id');
        }
        return $group->toArray();
    }
}
