<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\User;
use App\Models\Role;
use App\Models\Group;

class UserRepository implements RepositoryInterface
{
    use TraitUploadImage;

    public function __construct(
        User $model,
        Group $group,
        Role $role
    ) {
        $this->model = $model;
        $this->group = $group;
        $this->role = $role;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->exceptMe()
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->exceptMe()->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);

        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        unset($data['_method']);
        unset($data['_token']);
        $model = $this->model->findOrFail($id);
        if ($data['password'] !== null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        $nama_file = $this->photoUploaded($data, 'users', $model->photo_profile);
        $data['photo_profile'] = $nama_file;
        $model->update($data);

        if ($data['role_id'] !== null) {
            $model->syncRoles($data['role_id']);
        }

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $data['password'] = bcrypt($data['password']);
        $nama_file = $this->photoUploaded($data, 'users');
        $data['photo_profile'] = $nama_file;
        $model = $this->model->create($data);

        $model->assignRole($data['role_id']);

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }

    public function getGroups()
    {
        return $this->group->pluck('name', 'id');
    }

    public function getRoles()
    {
        return $this->role->pluck('name', 'id');
    }
}
