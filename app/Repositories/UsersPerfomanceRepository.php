<?php

namespace App\Repositories;

use App\Models\Group;
use App\Models\Institution;
use App\Models\MainHabits;
use Generator\Interfaces\RepositoryInterface;
use App\Models\RealitationBasic;
use App\Models\ScoreMainHabits;
use App\Models\ScoreSubMainHabits;
use App\Providers\Collection;
use App\Models\User;
use Auth;

class UsersPerfomanceRepository implements RepositoryInterface
{
    public function __construct(
        User $model,
        Group $group,
        Institution $institution,
        MainHabits $mainHabits,
        RealitationBasic $realitationBasic,
        ScoreMainHabits $scoreMain,
        ScoreSubMainHabits $scoreSubMain
    ) {
        $this->model = $model;
        $this->group = $group;
        $this->mainHabits = $mainHabits;
        $this->realitationBasic = $realitationBasic;
        $this->scoreMain = $scoreMain;
        $this->scoreSubMain = $scoreSubMain;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null, $print = false)
    {
        $user = Auth::user();
        $institution = $user->institution_id;

        $roles = $user->roles->pluck('name');
        
        if (is_null($institution)) {
            $groupUser = Group::pluck('id')->toArray();
        } else {
            $groupUser = Institution::find($institution)->group->pluck('id')->toArray();
        }
        
        $data = $this->model->where(function($query) use ($groupUser, $roles, $request) {
            if (!empty($roles->toArray()) and !in_array('admin', $roles->toArray())) {
                foreach (array_values($groupUser) as $group) {
                    $query->orWhere('group_id', $group);
                }
            }
            
            if (!is_null($request) and !is_null($request->get('name'))) {
                $query->where('name', 'like', '%'.$request->get('name').'%');
            }

            if (!is_null($request) and !is_null($request->get('group'))) {
                $query->where('group_id', 'like', '%'.$request->get('group').'%');
            }

            if (!is_null($request) and !is_null($request->get('bulan'))) {
                $query->whereHas('scoreMainHabits', function ($querys) use ($request) {
                    $querys->whereMonth('date', $request->get('bulan'));
                });
            }
        })->where('role_id', 2)->orderBy('id', 'ASC');

        return $print ? $data->get() : $data->paginate(20);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $parent = $model->sub_main_habits_id;
        $model->delete();

        return strval($parent);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $data;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $getHabits = $this->mainHabits->find($data['main_habits_id']);
        $data['date'] = $this->convertDate($data);
        $name = strtolower($getHabits->name);
        if (strpos($name, 'tertib mengikuti pengajian') !== false
            || strpos($name, 'tertib membaca') !== false
            || strpos($name, 'berpikir positif') !== false
            || strpos($name, 'beradab islami') !== false
            || strpos($name, 'berdakwah') !== false) {
            $this->insertGeneric($data, $getHabits);
        }

        if (strpos($name, 'tertib shalat') !== false || strpos($name, 'tertib sholat') !== false) {
            $this->insertSpesific($data, $getHabits);
        }

        return $data;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }

    public function getGroups()
    {
        return $this->group->pluck('name', 'id');
    }

    public function getMainHabits()
    {
        return $this->mainHabits->pluck('name', 'id');
    }

    public function convertDate($data)
    {
        return date('Y-m-d', strtotime($data['date']));
    }

    /**
     * Fungsi2 untuk insert.
     * There are two kind of insertion
     * 1. Generic just like insertGeneric()
     * 2. Spesific like  insertSpesific().
     */
    public function insertSpesific($data, $mainHabits, $byGroup = false)
    {
        if ($byGroup === false) {
            $getUser = $this->getUserFromGroup($data);
        } else {
            $getUser = $this->getUserByRole();
        }
        $dataScoreMainHabits = [];
        $dataScoreSubMainHabits = [];
        foreach ($getUser as $user) {
            $habitsFardhu = $mainHabits->subMainHabits()->whereIn('name', ['Maghrib', 'Isya', 'Subuh', 'Dhuhur', 'Ashar'])->get();

            $habitsSunnah = $mainHabits->subMainHabits()->whereIn('name', ['Tahajud', 'Dhuha'])->get();
            $totalScore = 0;

            foreach ($habitsFardhu as $subMainHabits) {
                $countRealitation = $this->realitationBasic
                ->whereDate('date', $data['date'])
                ->where('user_id', $user)
                ->where('value', '1')
                ->whereIn('habits_id', $subMainHabits->habits->pluck('id'))
                ->count();

                $habitsCounted     = $subMainHabits->habits->count();
                $fardhuHabits      = $subMainHabits->habits->where('isFardhu', 1)->pluck('id');
                
                $realitationFardhu = $this->realitationBasic
                ->whereDate('date', $data['date'])
                ->where('user_id', $user)
                ->whereIn('habits_id', $fardhuHabits)
                ->count();
                
                $score = $countRealitation / $habitsCounted * $realitationFardhu * 100;
                $totalScore += $score;

                $dataScoreSubMainHabits[] = [
                    'score' => $score,
                    'sub_main_habits_id' => $subMainHabits->id,
                    'date' => $data['date'],
                    'user_id' => $user,
                ];
            }

            foreach ($habitsSunnah as $subMainHabits) {
                $countRealitation = $this->realitationBasic
                ->whereDate('date', $data['date'])
                ->where('user_id', $user)
                ->whereIn('habits_id', $subMainHabits->habits->pluck('id'))
                ->count();

                $habitsCounted = $subMainHabits->habits->count();
                $score = $countRealitation / $habitsCounted * 100;
                $totalScore += $score;

                $dataScoreSubMainHabits[] = [
                    'score' => $score,
                    'sub_main_habits_id' => $subMainHabits->id,
                    'date' => $data['date'],
                    'user_id' => $user,
                ];
            }
            $totalHabits = $mainHabits->subMainHabits->count();
            $newTotalScore = $totalScore / $totalHabits;
            $dataScoreMainHabits[] = [
                'score' => $newTotalScore,
                'main_habits_id' => $data['main_habits_id'],
                'date' => $data['date'],
                'user_id' => $user,
            ];
        }

        $this->scoreMain->insert($dataScoreMainHabits);
        $this->scoreSubMain->insert($dataScoreSubMainHabits);

        return true;
    }

    public function insertGeneric($data, $mainHabits, $byGroup = false): Bool
    {
        if ($byGroup === false) {
            $getUser = $this->getUserFromGroup($data);
        } else {
            $getUser = $this->getUserByRole();
        }
        $countHabits = $this->countHabits($mainHabits);
        $getHabits = $this->getHabits($mainHabits);
        $dataScoreMainHabits = [];
        $dataScoreSubMainHabits = [];

        foreach ($getUser as $user) {
            $countRealitation = $this->realitationBasic
            ->whereDate('date', $data['date'])
            ->where('user_id', $user)
            ->whereIn('habits_id', $getHabits)
            ->count();

            $score = $countRealitation / $countHabits * 100;

            $dataScoreMainHabits[] = [
                'score' => $score,
                'main_habits_id' => $data['main_habits_id'],
                'date' => $data['date'],
                'user_id' => $user,
            ];

            foreach ($mainHabits->subMainHabits as $subMainHabits) {
                $dataScoreSubMainHabits[] = [
                    'score' => $score,
                    'sub_main_habits_id' => $subMainHabits->id,
                    'date' => $data['date'],
                    'user_id' => $user,
                ];
            }
        }

        $this->scoreMain->insert($dataScoreMainHabits);
        $this->scoreSubMain->insert($dataScoreSubMainHabits);

        return true;
    }

    /**
     * Fungsi2 pendukung.
     */
    public function getUserFromGroup($data)
    {
        $getGroup = $this->group->find($data['group_id']);

        return $getGroup->user->pluck('id');
    }

    public function getHabits($mainHabits)
    {
        $getSubMainHabits = $mainHabits->subMainHabits;
        $arrayHabits = [];

        foreach ($getSubMainHabits as $subMainHabits) {
            foreach ($subMainHabits->habits as $habits) {
                $arrayHabits[] = $habits->id;
            }
        }

        return $arrayHabits;
    }

    public function countHabits($mainHabits)
    {
        $getSubMainHabits = $mainHabits->subMainHabits;
        $countHabits = 0;
        foreach ($getSubMainHabits as $subMainHabits) {
            $countHabits += $subMainHabits->habits->count();
        }

        return $countHabits;
    }

    public function countHabitsFardhu($mainHabits)
    {
        $getSubMainHabits = $mainHabits->subMainHabits;
        $countHabits = 0;
        foreach ($getSubMainHabits as $subMainHabits) {
            $countHabits += $subMainHabits->habits->where('isFardhu', 1)->count();
        }

        return $countHabits;
    }

    public function getHabitsScore($request)
    {
    }

    public function getUserByRole()
    {
        return User::whereHas('roles', function ($q) {
            $q->where('name', 'santri');
        })->where('role_id', 2)->pluck('id');
    }

    public function getBulan()
    {
        return [
            '1' => 'Januari',
            '2' => 'Febuari',
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];
    }
}
