<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\SubMainHabits;
use App\Models\MainHabits;

class SubMainHabitsRepository implements RepositoryInterface
{
    public function __construct(
        SubMainHabits $model,
        MainHabits $main_habits
    ) {
        $this->model = $model;
        $this->main_habits = $main_habits;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('id', 'asc')
                    ->paginate(10);
    }

    public function getItemByParent($request)
    {
        $parent = $request->get('parent');
        $parent_record = $this->main_habits->findOrFail($parent);
        $data = $this->model->where('main_habits_id', $parent)
        ->filter($request)
        ->orderBy('id', 'asc')
        ->get();

        $response = [
            'detail' => $data,
            'parent' => $parent_record,
        ];

        return $response;
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $parent = $model->main_habits_id;
        $model->delete();

        return strval($parent);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $data['main_habits_id'];
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $this->main_habits->findOrFail($data['main_habits_id']);
        $this->model->create($data);

        return $data['main_habits_id'];
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
