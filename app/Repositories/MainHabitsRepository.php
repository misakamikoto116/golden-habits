<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\MainHabits;
use App\Models\SubMainHabits;
use App\Models\Habits;
use App\Models\RealitationBasic;

class MainHabitsRepository implements RepositoryInterface
{
    use TraitUploadImage;

    public function __construct(
        MainHabits $model
    ) {
        $this->model = $model;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('id', 'asc')
                    ->paginate(10);
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        $habits = Habits::where('main_habits_id', $id);
        $dataHabits = $habits->pluck('id');

        RealitationBasic::whereIn('habits_id', $dataHabits)->delete();
        $habits = $habits->delete();
        SubMainHabits::where('main_habits_id', $id)->delete();

        return $model->delete();
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        if (!isset($data['is_have_sub_main_habits'])) {
            $data['is_have_sub_main_habits'] = 0;
        }
        $model->update($data);

        return $model;
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        if (!isset($data['is_have_sub_main_habits'])) {
            $data['is_have_sub_main_habits'] = 0;
        }
        $model = $this->model->create($data);

        return $model;
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
