<?php

namespace App\Repositories;

use Generator\Interfaces\RepositoryInterface;
use App\Models\Group;
use App\Models\Institution;
use Exception;
use App\Models\User;
class GroupRepository implements RepositoryInterface
{
    public function __construct(
        Group $model,
        Institution $institution,
        User $user
    ) {
        $this->model = $model;
        $this->institution = $institution;
        $this->user = $user;
    }

    /**
     * ini untuk mengambil data keseluruhan
     * user di data repositori.
     *
     * @return Collection data list user
     */
    public function getItems($request = null)
    {
        return $this->model
                    ->filter($request)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(10);
    }

    public function getItemByParent($request)
    {
        $parent = $request->get('parent');
        $parent_record = $this->institution->findOrFail($parent);
        $data = $this->model->where('institution_id', $parent)
        ->filter($request)
        ->orderBy('updated_at', 'desc')
        ->get();

        $response = [
            'detail' => $data,
            'parent' => $parent_record,
        ];

        return $response;
    }

    /**
     * ini untuk mencari user berdasarkan id yang dicari.
     *
     * @param int $id
     *
     * @return object
     */
    public function findItem($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * ini untuk menghapus data berdasarkan id.
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    public function delete($id)
    {
        $model = $this->findItem($id);
        if($model->user->count() > 0){
            throw new Exception("Error, grup sudah memiliki user", 401);
        }
        $parent = $model->institution_id;
        $model->delete();

        return strval($parent);
    }

    /**
     * update data berdasarkan id dan data
     * didapat dari variable request.
     *
     * @param [type] $id   [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    public function update($id, $data)
    {
        $model = $this->model->findOrFail($id);
        $model->update($data);

        return $data['institution_id'];
    }

    /**
     * menambahkan data berdasarkan request.

     *
     * @param [type] $request [escription]
     *
     * @return [type] [description]
     */
    public function insert($data)
    {
        $this->institution->findOrFail($data['institution_id']);
        $model = $this->model->create($data);

        if (!empty($data['add_user_group'])) {
            $user = $this->user->create([
                'name' => $data['user_name'],
                'email' => $data['user_email'],
                'password' => bcrypt($data['user_password']),
                'group_id' => $model->id,
                'role_id' => 3
            ]);
            $user->assignRole(3);
        }
        
        return $data['institution_id'];
    }

    /**
     * ini berfungisi untuk melakukan filter terhadap
     * data yang akan diambil dan ditampilkan kepada
     * user nantinya.
     *
     * @param array $data
     */
    public function filter($request)
    {
        return $this->getItems($request);
    }
}
