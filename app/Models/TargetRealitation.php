<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TargetRealitation extends Model
{
    protected $table = 'target_realitation';

    protected $fillable = [
        'target', // generik, dia bisa jadi target hatam, atau target lainnya
        'main_habits_id',
        'priod',
        'result_target',
        'jumlah_khatam',
        'bulan',
        'user_id',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }
}
