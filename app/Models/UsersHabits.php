<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersHabits extends Model
{
    protected $fillable = [
        'user_profile_id',
        'habits_id',
        'description',
        'date',
    ];

    public function usersProfile()
    {
        return $this->belongsTo(User::class, 'user_profile_id');
    }

    public function habits()
    {
        return $this->belongsTo(Habits::class, 'habits_id');
    }
}
