<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Habits extends Model
{
    protected $table = 'habits';
    protected $fillable = [
        'name',
        'type',
        'periode_trigger',
        'main_habits_id',
        'sub_main_habits_id',
        'isFardhu',
    ];

    public function subMainHabits()
    {
        return $this->belongsTo(SubMainHabits::class, 'sub_main_habits_id');
    }

    public function realitationBasic()
    {
        return $this->hasMany(RealitationBasic::class, 'habits_id');
    }

    public function getTipeAttribute()
    {
        if ($this->getAttribute('type') === 0) {
            return 'Basic';
        } else {
            return 'Custom';
        }
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('name')) {
            $query->where('name', 'LIKE', '%'.$request->get('name').'%');
        }

        if ($request->has('tipe')) {
            $query->where('type', 'LIKE', '%'.$request->get('tipe').'%');
        }

        if ($request->has('periode_trigger')) {
            $query->where('periode_trigger', 'LIKE', '%'.$request->get('periode_trigger').'%');
        }

        return $query;
    }
}
