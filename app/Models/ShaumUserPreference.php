<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShaumUserPreference extends Model
{
    protected $table = 'shaum_user_preferences';

    protected $fillable = [
        'shaum_preferences_name',
        'shaum_daud_position',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
