<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'title',
        'message',
        'date',
        'isSuccess'
    ];

    public function getSuccessFormattedAttribute()
    {
        if($this->getAttribute('isSuccess')){
            return "<span class='label label-success'>Success</span>";
        }
        return "<span class='label label-danger'>Failed</span>";
    }

    public function scopeFilter($query, $request)
    {
        if($request === null){
            return $query;
        }

        if($request->get('tanggal') != null){
            return $query->whereDate('date', $request->get('tanggal'));
        }
    }
}
