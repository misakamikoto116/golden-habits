<?php

namespace App\Models;

use Spatie\Permission\Models\Role as Roles;

class Role extends Roles
{
    protected $table = 'roles';
    protected $fillable = ['name', 'guard_name'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];

    /** SCOPES */
    public function scopeFilter($query, $request)
    {
        if (!isset($request)) {
            return $query;
        }

        if ($request->get('nama') !== null) {
            $query->where('name', 'like', '%'.$request->get('nama').'%');
        }

        if ($request->get('nama_pelindung') != null) {
            $query->where('guard_name', 'LIKE', '%'.$request->get('nama_pelindung').'%');
        }

        if ($request->get('tanggal_pembuatan') != null) {
            $query->whereDate('created_at', date('Y-m-d', strtotime($request->get('tanggal_pembuatan'))));
        }

        return $query;
    }
}
