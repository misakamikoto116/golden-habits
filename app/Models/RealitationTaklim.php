<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealitationTaklim extends Model
{
    protected $table = 'realitation_taklim';
    protected $fillable = [
        'place',
        'key_speaker',
        'theme',
        'start_time',
        'end_time',
        'duration',
        'main_habits_id',
        'user_id',
        'date',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
