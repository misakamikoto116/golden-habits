<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'shaum_daud_position',
        'gender',
        'phone',
        'photo_profile',
        'fcm_token',
        'institution_id',
        'group_id',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = [
        'roles', 'group', 'scoreMainHabits',
    ];

    protected $appends = [
        'full_url_photo_profile',
    ];

    /**
     * Relationshit.
     *
     * @return models
     */
    public function institution()
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function scoreMainHabits()
    {
        return $this->hasMany(ScoreMainHabits::class, 'user_id');
    }

    public function scopeFilterScore($query, $request)
    {
        $bulan = date('m');
        if ($request != null) {
            if ($request->get('bulan') != null) {
                $bulan = $request->get('bulan');
            }
        }

        $query = $query->with(['scoreMainHabits' => function ($query) use ($bulan) {
            return $query->whereMonth('date', $bulan);
        }]);

        return $query;
    }

    public function scopeFilter($query, $request)
    {
        $current_roles = Auth::user()->roles->first()->name;

        if ($current_roles === 'user admin') {
            $grup = Auth::user()->group_id;
            $query->where('group_id', $grup);
        }

        if ($current_roles === 'admin instansi') {
            $institution_id = Auth::user()->institution_id;
            $institution    = Institution::find($institution_id)->group->pluck('name', 'id');
            $conditions     = (count($institution->toArray()) > 0) ? "group_id = " . implode(" OR group_id = ", array_keys($institution->toArray())) : "";
            
            $query->whereRaw($conditions);
        }

        if ($request === null) {
            return $query;
        }

        if ($request->get('name') != null) {
            $query->where('name', 'LIKE', '%'.$request->get('name').'%');
        }

        if ($request->get('group') != null) {
            $query->whereHas('group', function ($query) use ($request) {
                return $query->where('name', 'LIKE', '%'.$request->get('group').'%');
            });
        }

        return $query;
    }

    public function scopeExceptMe($query)
    {
        return $query->where('id', '!=', Auth::user()->id)->whereHas('roles', function ($query) {
            return $query->where('name', '!=', 'admin');
        });
    }

    public function getPhotoProfileFormattedAttribute()
    {
        if ($this->getAttribute('photo_profile')) {
            return 'storage/users/'.$this->getAttribute('photo_profile');
        } else {
            return 'adminlte/dist/img/user2-160x160.jpg';
        }
    }

    public function getFullUrlPhotoProfileAttribute()
    {
        return asset($this->getPhotoProfileFormattedAttribute());
    }

    public function realizationTarget()
    {
        return $this->hasMany(TargetRealitation::class, 'user_id');
    }

    public function getScoreHabits($bulan)
    {
        $data = [];
        $scoreMainHabits = ScoreMainHabits::where('user_id', $this->id)
                                ->whereMonth('date', $bulan)
                                ->whereYear('date', date('Y'))
                                ->get();
        foreach ($scoreMainHabits as $mainHabits) {
            $data[$mainHabits->main_habits_id] = "$mainHabits->score";
        }

        return $data;
    }
}
