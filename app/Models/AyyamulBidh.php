<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AyyamulBidh extends Model
{
    protected $table = 'ayyamul_bidh';
    protected $fillable = [
        'date_hijriyah',
        'month_hijriyah',
        'year_hijriyah',
        'date_masehi',
    ];

    public function getTanggalHijriyahAttribute()
    {
        return $this->getAttribute('date_hijriyah').'-'.$this->getAttribute('month_hijriyah').'-'.$this->getAttribute('year_hijriyah');
    }

    public function getTanggalMasehiAttribute()
    {
        return date('d-m-Y', strtotime($this->getAttribute('date_masehi')));
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->get('tanggal_masehi') !== null) {
            $query->where('date_masehi', 'LIKE', '%'.$request->get('tanggal_masehi').'%');
        }

        if ($request->get('bulan') !== null) {
            $query->whereMonth('date_masehi', $request->get('bulan'));
        }

        if ($request->get('tahun') !== null) {
            $query->whereYear('date_masehi', $request->get('tahun'));
        }

        return $query;
    }
}
