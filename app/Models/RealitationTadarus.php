<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealitationTadarus extends Model
{
    protected $table = 'realitation_tadarus';
    protected $fillable = [
        'start_time',
        'end_time',
        'duration',
        'juz',
        'date',
        'place',
        'surah',
        'ayat',
        'isHatam',
        'main_habits_id',
        'user_id',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTanggalFormattedAttribute()
    {
        return date('D, d F Y', strtotime($this->getAttribute('date')));
    }

    public function scopeFilter($query, $request)
    {
        if (!isset($request)) {
            return $query;
        }

        if ($request->get('nama') !== null) {
            $query->whereHas('user', function ($query) use ($request) {
                return $query->where('name', 'like', '%'.$request->get('nama').'%');
            });
        }

        if ($request->get('tanggal') !== null) {
            $query->whereDate('date', date('Y-m-d', strtotime($request->get('tanggal'))));
        }

        if ($request->get('juz') !== null) {
            $query->where('juz', $request->get('juz'));
        }

        if ($request->get('surah') !== null) {
            $query->where('surah', 'LIKE', '%'.$request->get('surah').'%');
        }

        if ($request->get('ayat') !== null) {
            $query->where('ayat', $request->get('ayat'));
        }

        if ($request->get('place') !== null) {
            $query->where('place', 'LIKE', '%'.$request->get('place').'%');
        }

        if ($request->get('group') !== null) {
            $query->whereHas('user', function ($query) use ($request) {
                return $query->where('group_id', $request->get('group'));
            });
        }

        return $query;
    }
}
