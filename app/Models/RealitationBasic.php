<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealitationBasic extends Model
{
    protected $table = 'relation_basics'; //typo
    protected $fillable = [
        'value',
        'habits_id',
        'user_id',
        'date',
    ];

    public function habits()
    {
        return $this->belongsTo(Habits::class, 'habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDateFormattedAttribute()
    {
        return date('d F Y', strtotime($this->getAttribute('date')));
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->get('nama') !== null) {
            $query->whereHas('user', function ($query) use ($request) {
                return $query->where('name', 'like', '%'.$request->get('nama').'%');
            });
        }

        if ($request->get('tanggal') !== null) {
            $query->whereDate('date', date('Y-m-d', strtotime($request->get('tanggal'))));
        }

        if ($request->get('habits') !== null) {
            $query->whereHas('habits', function ($query) use ($request) {
                return $query->where('name', 'like', '%'.$request->get('habits').'%');
            });
        }

        if ($request->get('grup') != null) {
            $query->whereHas('user', function ($query) use ($request) {
                return $query->where('group_id', $request->get('grup'));
            });
        }

        return $query;
    }
}
