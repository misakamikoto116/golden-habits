<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainHabits extends Model
{
    protected $table = 'main_habits';
    protected $fillable = [
        'name',
        'is_have_sub_main_habits',
    ];

    public function subMainHabits()
    {
        return $this->hasMany(SubMainHabits::class, 'main_habits_id');
    }

    public function habits()
    {
        return $this->hasMany(Habits::class, 'main_habits_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('name')) {
            $query->where('name', 'LIKE', '%'.$request->get('name').'%');
        }

        return $query;
    }
}
