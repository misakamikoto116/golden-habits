<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealitationZis extends Model
{
    protected $table = 'realitation_zis';

    protected $fillable = [
        'nominal_income',
        'nominal_realization',
        'percentage_realization',
        'main_habits_id',
        'user_id',
        'date',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTanggalFormattedAttribute()
    {
        return date('d F Y', strtotime($this->getAttribute('date')));
    }

    public function getNilaiZisAttribute()
    {
        return $this->getAttribute('nominal_income') * $this->getAttribute('percentage_realization');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->get('tanggal') !== null) {
            $query->whereDate('date', date('Y-m-d', strtotime($request->get('tanggal'))));
        }

        if ($request->get('bulan') !== null) {
            $query->whereMonth('date', $request->get('bulan'));
        }

        if ($request->get('nama') !== null) {
            $query->whereHas('user', function ($query) use ($request) {
                return $query->where('name', 'like', '%'.$request->get('nama').'%');
            });
        }

        if ($request->get('pendapatan') !== null) {
            $query->where('nominal_income', 'LIKE', '%'.$request->get('pendapatan').'%');
        }

        if ($request->get('norma_zis') !== null) {
            $query->where('percentage_realization', 'LIKE', '%'.$request->get('norma_zis').'%');
        }

        if ($request->get('nominal_realization') !== null) {
            $query->where('zis_dibayar', 'LIKE', '%'.$request->get('nominal_realization').'%');
        }

        return $query;
    }
}
