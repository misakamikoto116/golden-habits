<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name',
        'group_code',
        'institution_id',
    ];

    protected $with = [
        'institutions',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($grup) {
                if ($grup->user()->count() > 0) {
                    return false;
                }
            }
        );
    }

    public function user()
    {
        return $this->hasMany(User::class, 'group_id');
    }

    public function institutions()
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function getAdminGroupAttribute()
    {
        return User::where(['group_id' => $this->getAttribute('id'), 'role_id' => 3])->get();   
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('nama')) {
            $query->where('name', 'LIKE', '%'.$request->get('nama').'%');
        }

        if ($request->has('kode_grup')) {
            $query->where('group_code', 'LIKE', '%'.$request->get('kode_grup').'%');
        }

        return $query;
    }
}
