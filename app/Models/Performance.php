<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    protected $table = 'performances';

    protected $fillable = [
        'month',
        'notes',
        'user_id',
    ];

    public function performanceDetail()
    {
        return $this->hasMany(PerformanceDetail::class, 'performances_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('notes')) {
            $query->where('notes', 'LIKE', '%'.$request->get('notes').'%');
        }

        return $query;
    }

    public function getBulanAttribute()
    {
        $bulan = $this->getAttribute('month');

        return date('F', strtotime($bulan));
    }
}
