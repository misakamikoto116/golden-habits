<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceDetail extends Model
{
    protected $table = 'performance_details';

    protected $fillable = [
        'main_habits_id',
        'skor',
        'performances_id'
    ];

    public function mainHabits() {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function performance() {
        return $this->belongsTo(Performance::class, 'performances_id');
    }
}
