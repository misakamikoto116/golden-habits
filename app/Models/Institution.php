<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $fillable = [
        'name',
        'address',
        'kota',
        'provinsi',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($grup) {
                if ($grup->group()->count() > 0) {
                    return false;
                }
            }
        );
    }


    public function group()
    {
        return $this->hasMany(Group::class, 'institution_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('nama')) {
            $query->where('name', 'LIKE', '%'.$request->get('nama').'%');
        }

        if ($request->has('alamat')) {
            $query->where('address', 'LIKE', '%'.$request->get('alamat').'%');
        }

        return $query;
    }

    public function getAdminAttribute()
    {
        return User::where(['institution_id' => $this->getAttribute('id'), 'role_id' => 4])->get();   
    }
}
