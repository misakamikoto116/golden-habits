<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubMainHabits extends Model
{
    protected $table = 'sub_main_habits';
    protected $with = ['habits'];
    protected $fillable = [
        'name',
        'main_habits_id',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function habits()
    {
        return $this->hasMany(Habits::class, 'sub_main_habits_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->has('name')) {
            $query->where('name', 'LIKE', '%'.$request->get('name').'%');
        }

        return $query;
    }
}
