<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreMainHabits extends Model
{
    protected $fillable = [
        'score',
        'main_habits_id',
        'date',
        'user_id',
    ];

    public function mainHabits()
    {
        return $this->belongsTo(MainHabits::class, 'main_habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request === null) {
            return $query;
        }

        if ($request->get('month') != null) {
            $query->whereMonth('date', $request->get('month'));
        }

        if ($request->get('tahun') != null) {
            $query->whereYear('date', $request->get('tahun'));
        }

        return $query;
    }
}
