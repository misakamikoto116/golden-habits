<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreSubMainHabits extends Model
{
    protected $fillable = [
        'score',
        'sub_main_habits_id',
        'date',
        'user_id',
    ];

    public function subMainHabits()
    {
        return $this->belongsTo(SubMainHabits::class, 'sub_main_habits_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
