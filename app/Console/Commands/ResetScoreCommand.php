<?php

namespace App\Console\Commands;

use App\Models\ScoreMainHabits;
use App\Models\ScoreSubMainHabits;
use Illuminate\Console\Command;

class ResetScoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset score';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ScoreMainHabits::where('score', 0)->delete();
        ScoreSubMainHabits::where('score', 0)->delete();
        $this->info('berhasil reset score');
    }
}
