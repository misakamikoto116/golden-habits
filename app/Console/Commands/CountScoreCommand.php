<?php

namespace App\Console\Commands;

use App\Models\MainHabits;
use Illuminate\Console\Command;
use App\Repositories\UsersPerfomanceRepository as UsersRepo;

class CountScoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'count:score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hitung Skor User -2 hari dari sekarang';

    /**
     * Create a new command instance.
     */
    public function __construct(UsersRepo $userRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mainHabits = MainHabits::get();
        $countData = 0;
        foreach ($mainHabits as $mainHabit) {
            $data['date'] = date('Y-m-d', (strtotime('-2 days')));
            $data['main_habits_id'] = $mainHabit->id;

            $name = strtolower($mainHabit->name);
            if (strpos($name, 'tertib membaca') !== false
                    || strpos($name, 'berpikir positif') !== false
                    || strpos($name, 'beradab islami') !== false
                    || strpos($name, 'berdakwah') !== false) {
                $this->userRepo->insertGeneric($data, $mainHabit, true);
                ++$countData;
            }

            if (strpos($name, 'tertib shalat') !== false) {
                $this->userRepo->insertSpesific($data, $mainHabit, true);
                ++$countData;
            }
        }
        $this->info('Sukses menghitung skor sebanyak: '.$countData.' main habits.');
    }
}
