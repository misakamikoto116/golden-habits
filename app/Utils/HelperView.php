<?php

namespace App\Utils;

use App\Models\Group;

class HelperView {

    public static function group($id)
    {
        return Group::where('id', $id)->first()->name;
    }

}