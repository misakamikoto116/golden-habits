<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\PengamalanHabitsUmumRepository;
use App\Repositories\UserRepository;

class UsersController extends Controller
{
    public function __construct(UserRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Manajemen User';
        $this->request = UserRequest::class;
        $this->module = 'user';
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'group' => 'select'
            ],
            'group' => $this->model->getGroups()
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function embedData()
    {
        return [
            'groups' => $this->model->getGroups(),
            'roles' => $this->model->getRoles(),
        ];
    }
}
