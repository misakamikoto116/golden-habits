<?php

namespace App\Http\Controllers;

use App\Http\Requests\AyyamulBidhRequest;
use App\Repositories\AyyamulBidhRepository;

class AyyamulBidhController extends Controller
{
    public function __construct(AyyamulBidhRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Ayyamul Bidh';
        $this->module = 'ayyamul_bidh';
        $this->request = AyyamulBidhRequest::class;
        $this->view = [
            'filter' => [
                'tanggal_masehi' => 'date_normal',
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }
}
