<?php

namespace App\Http\Controllers;

use App\Repositories\RealitationTadarusRepository;

class RealitationTadarusController extends Controller
{
    public function __construct(RealitationTadarusRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Pengamalan Tadarus';
        $this->module = 'pengamalan_tadarus';
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'tanggal' => 'date',
                'juz' => 'number',
                'surah' => 'text',
                'ayat' => 'number',
                'place' => 'text',
                'group' => 'select',
            ],
            'group' => $this->model->getGroup(),
            'create_unactive' => true,
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }
}
