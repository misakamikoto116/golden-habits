<?php

namespace App\Http\Controllers;

use App\Repositories\UsersPerfomanceRepository;
use Illuminate\Http\Request;

class UsersPerfomanceController extends Controller
{
    public function __construct(UsersPerfomanceRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Performa User';
        $this->module = 'user_performance';
        $this->view = [
            'filter' => [
                'name' => 'text',
                'group' => 'select',
                'bulan' => 'select',
            ],
            'group' => $this->model->getGroups(),
            'bulan' => $this->model->getBulan(),
            'mainHabits' => $this->model->getMainHabits(),
            'create_unactive' => true,
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function embedData()
    {
        return [
            'main_habits' => $this->model->getMainHabits(),
            'groups' => $this->model->getGroups(),
        ];
    }

    public function getHabitsScore(Request $request)
    {
        $this->model->getHabitsScore($request);
    }

    public function print(Request $request)
    {
        $data = $this->model->getItems($request, true);
        $mainHabits = $this->model->getMainHabits();
        return view('admin.user_performance.print', compact('data', 'mainHabits'));
    }
}
