<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Repositories\GroupRepository;
use Session;

class GroupController extends Controller
{
    public function __construct(GroupRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Groups';
        $this->request = GroupRequest::class;
        $this->module = 'group';
    }

    public function index()
    {
        $request = request();
        $data = $this->model->getItemByParent($request);

        return parent::index()->with([
            'items' => $data['detail'],
            'parent' => $data['parent'],
            'filter' => [
                'nama' => 'text',
                'kode_grup' => 'text',
            ],
            'custom_url_create' => 'parent='.$request->get('parent'),
            'has_parent' => $request->get('parent'),
        ]);
    }

    public function redirectFail($actionFrom, \Illuminate\Http\Request $request, $exception = null)
    {
        Session::flash('type', 'danger');
        return redirect()->back()->withMessage('Tidak dapat menghapus lembaga yang memiliki user');
    }

    public function redirectSuccess($actionFrom, Request $request, $result = null)
    {
        if ($actionFrom == 'delete') {
            return redirect()->back()->withMessage('Berhasil menghapus data');
        } else {
            return redirect('/group?parent='.$result)->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }

}
