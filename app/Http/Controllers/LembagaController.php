<?php

namespace App\Http\Controllers;

use App\Repositories\LembagaRepository;
use App\Http\Requests\LembagaRequest;
use App\Http\Requests\InstitusiUserRequest;
use Session;
use App\Models\User;

class LembagaController extends Controller
{
    // protected $role = 'admin';

    public function __construct(LembagaRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Manajemen Lembaga';
        $this->request = LembagaRequest::class;
        $this->module = 'lembaga';
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'alamat' => 'text',
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function redirectFail($actionFrom, \Illuminate\Http\Request $request, $exception = null)
    {
        Session::flash('type', 'danger');

        return redirect()->back()->withMessage('Tidak dapat menghapus lembaga yang memiliki user');
    }

    public function user($id)
    {
        $user   = User::where('institution_id', $id)->first();
        $action = is_null($user) ? 'create' : 'update';
        return view('admin.lembaga.user', compact('id', 'user', 'action'));
    }

    public function userstore(InstitusiUserRequest $request, $id, $action)
    {
        if ($action == 'create') {
            $user = User::create([
                'name'           => $request->name,
                'email'          => $request->email,
                'password'       => bcrypt($request->password),
                'institution_id' => $id,
                'role_id'        => 4
            ]);
            $user->assignRole(4);
        } else {
            $user = User::where('institution_id', $id)->first();
            $user->update([
                'name'  => $request->name,
                'email' => $request->email
            ]);
        }

        return redirect()->back()->with('success', 'Berhasil!');
    }
}
