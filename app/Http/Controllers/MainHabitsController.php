<?php

namespace App\Http\Controllers;

use App\Http\Requests\MainHabitsRequest;
use App\Repositories\MainHabitsRepository;

class MainHabitsController extends Controller
{
    // protected $role = 'admin';

    public function __construct(MainHabitsRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Main Habits';
        $this->request = MainHabitsRequest::class;
        $this->module = 'main_habits';
        $this->view = [
            'filter' => ['name' => 'text'],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }
}
