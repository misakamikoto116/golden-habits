<?php

namespace App\Http\Controllers;

use App\Repositories\RealitationZisRepository;

class RealitationZisController extends Controller
{
    public function __construct(RealitationZisRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Pengamalan Zakat Infaq Sedekah';
        $this->module = 'pengamalan_zis';
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'pendapatan' => 'amount',
                'norma_zis' => 'number',
                'zis_dibayar' => 'amount',
                'tanggal' => 'date',
                'group' => 'select',
            ],
            'group' => $this->model->getGroup(),
            'create_unactive' => true,
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }
}
