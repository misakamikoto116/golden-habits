<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MainHabits;
use Illuminate\Http\Request;
use App\Models\SubMainHabits;
use App\Models\Habits;
use Auth;

class HabitsController extends Controller
{
    use TraitResponse;

    public function getListMainHabits()
    {
        $data = MainHabits::orderBy('id')->get();
        if ($data->count() >= 1) {
            return $this->response(true, 'Berhasil mendapatkan data', $data);
        }

        return $this->response(false, 'Data tidak ditemukan!');
    }

    public function getListSubMainHabits(Request $request)
    {
        $id = $request->get('main_habits_id');
        $data = SubMainHabits::where('main_habits_id', $id)->get();
        if ($data->count() >= 1) {
            return $this->response(true, 'Berhasil mendapatkan data', $data);
        }

        return $this->response(false, 'Data tidak ditemukan!');
    }

    public function getListSubMainHabitsAll(Request $request)
    {
        $id = $request->get('main_habits_id');
        $user_id = Auth::user()->id;
        if ($request->get('with_realitation_basic') === 'true') {
            $data = SubMainHabits::select(['id', 'name'])->with(['habits' => function ($query) use ($user_id, $request) {
                return $query->with(['realitationBasic' => function ($query) use ($user_id, $request) {
                    $query = $query->where('user_id', $user_id);

                    if ($request->get('date')) {
                        return $query->whereDate('date', $request->get('date'));
                    }

                    return $query;
                }]);
            }])->where('main_habits_id', $id)->orderBy('id')->get();
        } else {
            $data = SubMainHabits::select(['id', 'name'])->with(['habits' => function ($query) use ($request) {
                return $query->select(['name', 'type', 'periode_trigger', 'sub_main_habits_id']);
            }])->where('main_habits_id', $id)->orderBy('id')->get();
        }

        if ($data->count() >= 1) {
            return $this->response(true, 'Berhasil mendapatkan data sub habits dan habits nya', $data);
        }

        return $this->response(false, 'Data tidak ditemukan!');
    }

    public function getListHabits(Request $request)
    {
        $id = $request->get('sub_main_habits_id');
        $data = Habits::where('sub_main_habits_id', $id)->orderBy('id')->get();
        if ($data->count() >= 1) {
            return $this->response(true, 'Berhasil mendapatkan data', $data);
        }

        return $this->response(false, 'Data tidak ditemukan!');
    }
}
