<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HabitsPackages;
use App\Models\HabitsPackagesDetail;
use Illuminate\Http\Request;
use App\Models\Habits;
use App\Models\ScoreMainHabits;
use Illuminate\Support\Facades\Auth;
use App\Models\UsersHabits;
use Exception;
use Validator;

class PackageHabitsController extends Controller
{
    use TraitResponse;

    public function index()
    {
        $item = HabitsPackages::select(['id', 'name', 'image'])->get();

        return $this->response(true, 'Berhasil mendapatkan data paket habits', $item);
    }

    public function detail(Request $request)
    {
        $id = $request->get('id');
        $item = HabitsPackagesDetail::select(['habits_packages_id', 'name'])->where('habits_packages_id', $id)->get();

        if ($item->count() > 0) {
            return $this->response(true, 'Berhasil mendapatkan data detail paket habits', $item);
        } else {
            return $this->response(false, 'Gagal mendapatkan data Habits Package Detail, Data tidak ditemukan', null);
        }
    }

    public function detailWithHabits(Request $request)
    {
        $id = $request->get('habits_packages_id');
        $item = HabitsPackagesDetail::with('habits')->select(['id', 'name'])->where('habits_packages_id', $id)->get();

        if ($item->count() > 0) {
            return $this->response(true, 'Berhasil mendapatkan data detail paket habits', $item);
        } else {
            return $this->response(false, 'Gagal mendapatkan data Habits Package Detail, Data tidak ditemukan', null);
        }
    }

    public function habits(Request $request)
    {
        $id = $request->get('habits_packages_detail_id');
        $item = Habits::select(['id', 'name', 'type', 'detail'])->where('habits_packages_detail_id', $id)->get();

        if ($item->count() > 0) {
            return $this->response(true, 'Berhasil mendapatkan data habits', $item);
        } else {
            return $this->response(false, 'Gagal mendapatkan data habits, Data tidak ditemukan', null);
        }
    }

    public function saveUserHabits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'habits_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->response(false, $validator->errors(), null);
        }

        $data = [
            'user_profile_id' => Auth::user()->id,
            'habits_id' => $request->get('habits_id'),
            'description' => $request->get('habits_id'),
            'date' => date('Y-m-d H:i:s'),
        ];
        UsersHabits::create($data);

        return $this->response(true, 'Berhasil menyimpan data!', null);
    }

    public function getScoreMainHabits(Request $request)
    {
        try {
            $data = ScoreMainHabits::filter($request)->get();

            return $this->response(true, 'Berhasil mengambil data!', $data);
        } catch (Exception $e) {
            return $this->response(false, $e->getMessage());
        }
    }
}
