<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ScoreMainHabits;
use Illuminate\Support\Facades\Auth;

class ScoreController extends Controller
{
    use TraitResponse;

    public function getMyScore()
    {
        $user = Auth::user()->id;
        $data = ScoreMainHabits::where('user_id', $user)
                ->get()
                ->groupBy(function ($item) {
                    return date('m', strtotime($item->date));
                })->transform(function ($item) {
                    $score = $item->avg(function ($data) {
                        return $data['score'];
                    });

                    return $item->transform(function ($item) use ($score) {
                        $data['bulan'] = date('m', strtotime($item->date));
                        $data['tahun'] = date('Y', strtotime($item->date));
                        $data['bulanText'] = date('F Y', strtotime($item->date));
                        $data['skor'] = $score;

                        return $data;
                    })->unique('bulanText');
                });
        $uniqueData = [];
        foreach ($data as $items) {
            foreach ($items as $item) {
                $uniqueData[] = $item;
            }
        }

        return $this->response(true, 'Berhasil mendapatkan skor', $uniqueData);
    }

    public function getMyDetailScore($bulan, $tahun)
    {
        $user = Auth::user()->id;
        $data = ScoreMainHabits::where('user_id', $user)
                ->whereYear('date', $tahun)
                ->whereMonth('date', $bulan)
                ->get()
                ->groupBy('main_habits_id')->transform(function ($item) {
                    $score = $item->avg(function ($data) {
                        return $data['score'];
                    });

                    return $item->transform(function ($item) use ($score) {
                        $data['main_habits_id'] = $item->main_habits_id;
                        $data['main_habits_name'] = $item->mainHabits->name;
                        $data['skor'] = $score;

                        return $data;
                    })->unique('bulanText');
                });
        $uniqueData = [];
        foreach ($data as $items) {
            foreach ($items as $item) {
                $uniqueData[] = $item;
            }
        }

        return $this->response(true, 'Berhasil mendapatkan detail skor', $uniqueData);
    }
}
