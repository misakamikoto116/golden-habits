<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\User;
use App\Models\Group;
use App\Repositories\TraitUploadImage;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $successStatus = 200;
    use TraitResponse;
    use TraitUploadImage;

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $fcm_token = ['fcm_token' => request('fcm_token')];
            $user = Auth::user();
            $merged_user = collect($user)->except(['user_id', 'group_id', 'created_at', 'updated_at', 'email_verified_at', 'roles','score_main_habits']);
            User::find($user->id)->update($fcm_token);

            $response = [
                'status' => true,
                'message' => 'Berhasil login users',
                'data' => [
                    'access_token' => $user->createToken('nApp')->accessToken,
                    'user' => $merged_user,
                ],
            ];

            return response()->json($response);
        } else {
            return response()->json([
                'status' => false,
                'error' => 'Login gagal, Email atau password salah!',
            ], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'phone' => 'required',
            'shaum_daud_position' => 'nullable|boolean',
            'gender' => 'required',
            'fcm_token' => 'required',
        ]);
        if ($validator->fails()) {
            if ($validator->errors('email')) {
                return response()->json([
                    'status' => false,
                    'error' => 'Email sudah terdaftar, mohon masukkan email lainnya.',
                ], 401);
            } else {
                return response()->json([
                    'status' => false,
                    'error' => 'Harap periksa kembali inputan Anda!',
                ], 401);
            }
        }

        $input = $request->only(['name', 'email', 'phone', 'password', 'shaum_daud_position', 'gender', 'fcm_token']);
        $input['password'] = bcrypt($input['password']);

        $roles = Role::where('name', 'santri')->first();
        $input['role_id'] = $roles->id;

        $user = User::create($input);

        $user->assignRole('santri');
        $grup = Group::find($user->group_id);

        $getNewUser = USer::find($user->id);
        $merged_user = collect($getNewUser)->except(['user_id', 'created_at', 'updated_at', 'email_verified_at', 'roles']);

        $response = [
            'status' => true,
            'message' => 'Berhasil registrasi users',
            'data' => [
                'access_token' => $user->createToken('nApp')->accessToken,
                'group' => $grup = (isset($grup)) ? collect($grup)->except(['created_at', 'updated_at']) : null,
                'new_fcm_token' => request('fcm_token'),
                'user' => $merged_user,
            ],
        ];

        return response()->json($response);
    }

    public function postGroupCode(Request $request)
    {
        $user = Auth::user();
        $group_code = $request->get('group_code');

        $group = Group::where('group_code', $group_code)->first();
        if (empty($group)) {
            return $this->response(false, 'Gagal mengupdate group, Kode grup salah!');
        }

        User::find($user->id)->update(
            ['group_id' => $group->id]
        );

        return $this->response(true, 'Berhasil mengupdate group', $group);
    }

    public function postPhotoProfile(Request $request)
    {
        $user = Auth::user();

        $user_model = User::findOrFail($user->id);
        $data['image'] = $request->photo_profile;
        if (empty($data['image'])) {
            return $this->response(false, 'Gagal mengupdate photo profile, Photo tidak boleh kosong!');
        }

        $nama_file = $this->photoUploaded($data, 'users', $user_model->photo_profile);
        $uploadedFile['photo_profile'] = $nama_file;

        $user_model->update($uploadedFile);
        $latestModel = User::findOrFail($user->id);

        return $this->response(true, 'Berhasil mengupdate photo profile', asset($latestModel->photo_profile_formatted));
    }

    public function postProfileUser(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id.',id',
            'phone' => 'required',
            'gender' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors('email')) {
                return response()->json([
                    'status' => false,
                    'error' => 'Email sudah terdaftar, mohon masukkan email lainnya.',
                ], 401);
            } else {
                return response()->json([
                    'status' => false,
                    'error' => $validator->errors(),
                ], 401);
            }
        }

        $model = User::find($user->id)->update($request->all());

        return $this->response(true, 'Berhasil mengupdate profile', $model);
    }

    public function postChangePassword(Request $request)
    {
        $user = Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            $validator = Validator::make($request->all(), [
                'new_password' => 'required|min:6|same:new_password_confirmation',
                'new_password_confirmation' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return $this->response(false, $validator->errors(), null, 401);
            }

            $user = User::find($user->id)->update([
                'password' => bcrypt($request->new_password),
            ]);

            return $this->response(true, 'Password berhasil diubah');
        } else {
            return $this->response(false, 'Password lama belum benar', null, 401);
        }
    }

    public function postFcmToken(Request $request)
    {
        $user = Auth::user();

        if($request->get('fcm_token')){
            User::find($user->id)->update(['fcm_token' => $request->get('fcm_token')]);
            return $this->response(true, 'Token berhasil diupdate');
        }

        return $this->response(false, 'Tidak ada fcm token', null, 401);
    }
}
