<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use App\Models\TargetRealitation;
use Validator;
use Exception;

class TargetRealitationController extends Controller
{
    use TraitResponse;

    public function __construct(TargetRealitation $targetRealitation)
    {
        $this->targetRealitation = $targetRealitation;
    }

    public function postTargetRealitation(Request $request)
    {
        try {
            $user = Auth::user();

            $validator = Validator::make($request->all(), [
                'target' => 'numeric|required',
                'main_habits_id' => 'required',
                'priod' => 'required',
                'result_target' => 'nullable',
            ]);

            if ($validator->fails()) {
                return $this->response(false, $validator->errors(), null);
            }

            $data = $this->targetRealitation->create([
                'target' => $request->target,
                'main_habits_id' => $request->main_habits_id,
                'priod' => $request->priod,
                'result_target' => $request->result_target,
                'jumlah_khatam' => $request->jumlah_khatam,
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'user_id' => $user->id,
            ]);

            return $this->response(true, 'Berhasil menambah data!', $data);
        } catch (Exception $e) {
            return $this->response(false, $e->getMessage(), null);
        }
    }
}
