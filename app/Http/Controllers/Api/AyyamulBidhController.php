<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AyyamulBidh;
use Exception;
use Illuminate\Http\Request;

class AyyamulBidhController extends Controller
{
    use TraitResponse;

    public function getAyyamulBidh(Request $request)
    {
        try {
            $data = AyyamulBidh::filter($request)->get();

            return $this->response('true', 'Berhasil mengambil data Ayyamul Bidh', $data);
        } catch (Exception $e) {
            return $this->response('false', $e->message());
        }
    }
}
