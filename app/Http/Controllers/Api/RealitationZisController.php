<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RealitationZis;
use App\Models\ScoreMainHabits;
use Auth;
use Exception;
use Validator;
use DB;

class RealitationZisController extends Controller
{
    use TraitResponse;

    public function __construct(RealitationZis $realitationZis)
    {
        $this->realitationZis = $realitationZis;
    }

    public function postRealitationZis(Request $request)
    {
        try {
            $user = Auth::user();

            $validator = Validator::make($request->all(), [
                'nominal_income' => 'required|numeric',
                'nominal_realization' => 'required|numeric',
                'percentage_realization' => 'required|numeric',
                'main_habits_id' => 'required',
                'date' => 'nullable|date'
            ]);

            if ($validator->fails()) {
                return $this->response(false, $validator->errors(), null);
            }

            $data = $this->realitationZis->create([
                'nominal_income' => $request->nominal_income,
                'nominal_realization' => $request->nominal_realization,
                'percentage_realization' => $request->percentage_realization,
                'main_habits_id' => $request->main_habits_id,
                'date' => $request->date,
                'user_id' => $user->id,
            ]);

            $dateInput = $request->date ?? date('Y-m-d H:i:s');
            $month     = date('m', strtotime($dateInput));

            $zis = RealitationZis::where(['user_id' => $user->id])
                ->whereMonth('date', $month)
                ->select(
                    DB::raw('SUM(nominal_realization) AS realization'),
                    DB::raw('SUM(nominal_income*percentage_realization/100) AS income')
                )
                ->first();

            $skor = $zis->realization / $zis->income * 100;

            ScoreMainHabits::whereMonth('date', $month)
                ->updateOrCreate([
                    'main_habits_id' => $data->main_habits_id,
                    'user_id' => Auth::user()->id,
                    ], [
                    'date' => $dateInput,
                    'score' => $skor,
                ]);

            return $this->response(true, 'Berhasil menambah data!', $data);
        } catch (Exception $e) {
            return $this->response(false, $e->getMessage(), null);
        }
    }

    public function postRealitationZisBehavior(Request $request)
    {
        // try{
        //     $user = Auth::user();


        // } catch (Exception $e) {
        //     return $this->response(false, $e->getMessage(), null);
        // }
    }

    public function getRealitationZis(Request $request)
    {
        try {
            $user = Auth::user();
            $data = $this->realitationZis->where('user_id', $user->id)->filter($request)->get();

            return $this->response(true, 'Berhasil mengambil data!', $data);
        } catch (Exception $e) {
            return $this->response(false, $e->getMessage(), null);
        }
    }
}
