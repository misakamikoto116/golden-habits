<?php

namespace App\Http\Controllers\Api;

trait TraitResponse
{
    public function response($status, $message, $data = null, $code = 200)
    {
        $response = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];

        return response()->json($response, $code);
    }
}
