<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RealitationTadarusRequest;
use App\Http\Requests\RealitationTaklimRequest;
use Auth;
use App\Models\User;
use App\Models\RealitationBasic;
use App\Models\RealitationTadarus;
use App\Models\RealitationTaklim;
use App\Models\ScoreMainHabits;
use App\Models\TargetRealitation;
use App\Models\Habits;
use DateTime;

class RealitationController extends Controller
{
    use TraitResponse;

    public function postRealitationBasic(Request $request)
    {
        $user = Auth::user();

        $user_model = User::findOrFail($user->id);
        $habits_id = $request->get('habits_id');
        $value = $request->get('value');

        $data = [
            'user_id' => $user_model->id,
            'habits_id' => $habits_id,
            'value' => $value,
            'date' => $request->get('date') ?? date('Y-m-d H:i:s'),
        ];
        $insert = RealitationBasic::create($data);

        // fix select $realitationCount
        // fix get date
        $subHabis = Habits::find($habits_id)->subMainHabits;
        $checkMainHabits = $subHabis->mainHabits;
        if (strtolower($checkMainHabits->name) === 'tertib mengikuti pengajian') {
            $realitationCount = RealitationBasic::join('habits', 'relation_basics.habits_id', '=', 'habits.id')
                ->join('sub_main_habits', 'habits.sub_main_habits_id', '=', 'sub_main_habits.id')
                ->join('main_habits', 'sub_main_habits.main_habits_id', '=', 'main_habits.id')
                ->where(['relation_basics.user_id' => $user_model->id, 'main_habits.id' => $checkMainHabits->id])
                ->whereMonth('date', date('m', strtotime($data['date'])))->count();
            $userTarget = TargetRealitation::where([
                'main_habits_id' => $checkMainHabits->id,
                'bulan' => date('m', strtotime($data['date'])),
                'user_id' => $user_model->id,
            ])->latest()->first();
            $userTarget = $userTarget->target ?? 4;
            $skor = $realitationCount / ($subHabis->habits->count() * $userTarget) * 100;
            ScoreMainHabits::whereMonth('date', date('m', strtotime($data['date'])))
            ->updateOrCreate([
                'main_habits_id' => $checkMainHabits->id,
                'user_id' => Auth::user()->id,
                ], [
                'date' => $data['date'],
                'score' => $skor,
            ]);
        } else if (strtolower($checkMainHabits->name) === 'tertib puasa') {
            $realitationCount = RealitationBasic::join('habits', 'relation_basics.habits_id', '=', 'habits.id')
                ->join('sub_main_habits', 'habits.sub_main_habits_id', '=', 'sub_main_habits.id')
                ->join('main_habits', 'sub_main_habits.main_habits_id', '=', 'main_habits.id')
                ->where(['relation_basics.user_id' => $user_model->id, 'main_habits.id' => $checkMainHabits->id])
                ->whereMonth('date', date('m', strtotime($data['date'])))->count();
            $userTarget = TargetRealitation::where([
                'main_habits_id' => $checkMainHabits->id,
                'bulan' => date('m', strtotime($data['date'])),
                'user_id' => $user_model->id,
            ])->latest()->first();
            $userTarget = $userTarget->target ?? 4;
            $skor = $realitationCount / $userTarget * 100;
            ScoreMainHabits::whereMonth('date', date('m', strtotime($data['date'])))
            ->updateOrCreate([
                'main_habits_id' => $checkMainHabits->id,
                'user_id' => Auth::user()->id,
                ], [
                'date' => $data['date'],
                'score' => $skor,
            ]);
        }

        $data['realitation_id'] = $insert->id;
        if ($insert) {
            return $this->response(true, 'Berhasil menambah data!', $data);
        }

        return $this->response(false, 'Gagal menambah data!');
    }

    public function deleteRelationBasic(Request $request)
    {
        $id        = $request->get('realitation_basic_id');
        $data      = RealitationBasic::find($id);
        $habits_id = $data->habits_id;
        $month     = date('m', strtotime(date('Y-m-d H:i:s')));
        if ($data) {
            $data->delete();

            // add by saipul
            $subHabis = Habits::find($habits_id)->subMainHabits;
            $checkMainHabits = $subHabis->mainHabits;
            if (strtolower($checkMainHabits->name) === 'tertib mengikuti pengajian') {
                $realitationCount = RealitationBasic::join('habits', 'relation_basics.habits_id', '=', 'habits.id')
                    ->join('sub_main_habits', 'habits.sub_main_habits_id', '=', 'sub_main_habits.id')
                    ->join('main_habits', 'sub_main_habits.main_habits_id', '=', 'main_habits.id')
                    ->where(['relation_basics.user_id' => $user_model->id, 'main_habits.id' => $checkMainHabits->id])
                    ->whereMonth('date', date('m', strtotime($month)))->count();
                $userTarget = TargetRealitation::where([
                    'main_habits_id' => $checkMainHabits->id,
                    'bulan' => date('m', strtotime($month)),
                    'user_id' => $user_model->id,
                ])->latest()->first();
                $userTarget = $userTarget->target ?? 4;
                $skor = $realitationCount / ($subHabis->habits->count() * $userTarget) * 100;
                ScoreMainHabits::whereMonth('date', date('m', strtotime($month)))
                ->updateOrCreate([
                    'main_habits_id' => $checkMainHabits->id,
                    'user_id' => Auth::user()->id,
                    ], [
                    'date' => $month,
                    'score' => $skor,
                ]);
            }

            return $this->response(true, 'Berhasil menghapus realitation!');
        } else if (strtolower($checkMainHabits->name) === 'tertib puasa') {
            $realitationCount = RealitationBasic::join('habits', 'relation_basics.habits_id', '=', 'habits.id')
                ->join('sub_main_habits', 'habits.sub_main_habits_id', '=', 'sub_main_habits.id')
                ->join('main_habits', 'sub_main_habits.main_habits_id', '=', 'main_habits.id')
                ->where(['relation_basics.user_id' => $user_model->id, 'main_habits.id' => $checkMainHabits->id])
                ->whereMonth('date', date('m', strtotime($data['date'])))->count();
            $userTarget = TargetRealitation::where([
                'main_habits_id' => $checkMainHabits->id,
                'bulan' => date('m', strtotime($data['date'])),
                'user_id' => $user_model->id,
            ])->latest()->first();
            $userTarget = $userTarget->target ?? 4;
            $skor = $realitationCount / $userTarget * 100;
            ScoreMainHabits::whereMonth('date', date('m', strtotime($data['date'])))
            ->updateOrCreate([
                'main_habits_id' => $checkMainHabits->id,
                'user_id' => Auth::user()->id,
                ], [
                'date' => $data['date'],
                'score' => $skor,
            ]);
        }

        return $this->response(false, 'Gagal menghapus realitation! perhatikan parameter anda!');
    }

    public function postSubmitRealitationTaklim(RealitationTaklimRequest $request)
    {
        $startTime = new DateTime($request->get('start_time'));
        $endTime = new DateTime($request->get('end_time'));
        $duration = $startTime->diff($endTime);
        $duration = $duration->format('%H:%I:%S');

        $data = [
            'key_speaker' => $request->get('key_speaker'),
            'theme' => $request->get('theme'),
            'start_time' => $request->get('start_time'),
            'end_time' => $request->get('end_time'),
            'main_habits_id' => $request->get('main_habits_id'),
            'place' => $request->get('place'),
            'duration' => $duration,
            'date' => $request->get('date'),
            'user_id' => Auth::user()->id,
        ];

        $insert = RealitationTaklim::create($data);
        if ($insert) {
            return $this->response(true, 'Berhasil menambah data!', $insert);
        }

        return $this->response(false, 'Gagal menambah data!');
    }

    public function postSubmitRealitationTadarus(RealitationTadarusRequest $request)
    {
        $hatam = $request->get('isHatam');
        $juz = $request->get('juz');
        $startTime = new DateTime($request->get('start_time'));
        $endTime = new DateTime($request->get('end_time'));
        $duration = $startTime->diff($endTime);
        $duration = $duration->format('%H:%I:%S');
        $data = [
            'start_time' => $request->get('start_time'),
            'end_time' => $request->get('end_time'),
            'main_habits_id' => $request->get('main_habits_id'),
            'date' => $request->get('date'),
            'place' => $request->get('place'),
            'duration' => $duration,
            'juz' => $juz,
            'surah' => $request->get('surah'),
            'ayat' => $request->get('ayat'),
            'isHatam' => $hatam,
            'user_id' => Auth::user()->id,
        ];

        $userTarget = TargetRealitation::where([
            'user_id' => Auth::user()->id,
            'bulan' => date('m', strtotime($request->get('date'))),
            'main_habits_id' => $request->get('main_habits_id'),
        ])->latest()->first();
        $currentHatam = $userTarget->jumlah_khatam ?? 0;

        if ($hatam == 1 && isset($userTarget)) {
            $userTarget->update(['jumlah_khatam' => $currentHatam + 1]);
            ++$currentHatam;
            $juz = 0;
        }
        //Update/inse ke score main habits per user dan bulan
        /*
         * n = 30*jumlah khatam (dari targetRealitation)
         * a = jika hatam maka juz ke reset jadi 0 kalau tidak maka ambil dari row terakhir
         * skor = (n+a)/(30*Target hatam dari TargetRealitation)*100.
         */
        if($userTarget === null){
            return $this->response(401, "Error! user target tidak ada");
        }
        $n = 30 * $currentHatam;
        $skor = ($n + $juz) / (30 * $userTarget->target) * 100;

        ScoreMainHabits::whereMonth('date', date('m', strtotime($request->get('date'))))
        ->updateOrCreate([
            'main_habits_id' => $request->get('main_habits_id'),
            'user_id' => Auth::user()->id,
            ], [
            'date' => $request->get('date'),
            'score' => $skor,
        ]);

        $insert = RealitationTadarus::create($data);
        if ($insert) {
            return $this->response(true, 'Berhasil menambah data!', $insert);
        }

        return $this->response(false, 'Gagal menambah data!');
    }

    public function getRealitationTadarusMonthly()
    {
        $user = Auth::user()->id;
        $month = date('m');

        $data = RealitationTadarus::where('user_id', $user)->whereMonth('date', $month)->get();
        if ($data) {
            return $this->response(true, 'Berhasil mendapatkan data!', $data);
        }

        return $this->response(false, 'Gagal mendapatkan data!');
    }

    public function getRealitationTaklimMonthly()
    {
        $user = Auth::user()->id;
        $month = date('m');

        $data = RealitationTaklim::where('user_id', $user)->whereMonth('date', $month)->get();
        if ($data) {
            return $this->response(true, 'Berhasil mendapatkan data!', $data);
        }

        return $this->response(false, 'Gagal mendapatkan data!');
    }
}
