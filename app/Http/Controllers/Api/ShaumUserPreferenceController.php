<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShaumUserPreference;
use Auth;
use Exception;
use Validator;

class ShaumUserPreferenceController extends Controller
{
    use TraitResponse;

    public function __construct(ShaumUserPreference $shaumUserPreference)
    {
        $this->shaumUserPreference = $shaumUserPreference;
    }

    public function postShaumUserPreference(Request $request) {
        try {
            $user = Auth::user();

            $validator = Validator::make($request->all(), [
                'shaum_preferences_name' => 'required|string',
                'shaum_daud_position' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return $this->response(false, $validator->errors(), null);
            }   

            $data = $this->shaumUserPreference->create([
                'shaum_preferences_name' => $request->shaum_preferences_name,
                'shaum_daud_position' => $request->shaum_daud_position,
                'user_id' => $user->id
            ]);

            return $this->response(true, 'Berhasil menambah data!', $data);
        } catch (Exception $e) {
            return $this->response(false, $e->getMessage(), null);
        }
    }
}
