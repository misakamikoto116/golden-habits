<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Auth;
use App\Models\Group;

class ProfileController extends Controller
{
    public function __construct(
        User $model
    ) {
        $this->model = $model;
    }

    public function index()
    {
        $data = [
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
        ];

        return view('admin.profile.index')->with($data);
    }

    public function update($id = null)
    {
        $request = request();
        $data = $request->all();
        $id = Auth::user()->id;
        $request['id'] = $id;
        $this->validatation($request);

        unset($data['_method']);
        unset($data['_token']);
        $model = $this->model->findOrFail($id);
        if ($data['password'] !== null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        $model->update($data);

        if ($data['role_id'] !== null) {
            $model->syncRoles($data['role_id']);
        }
        $data['user_id'] = $id;
        unset($data['email'], $data['password'], $data['password2'], $data['institution_id'], $data['role_id']);

        return redirect()->back()->withMessage('Berhasil mengubah data profile!');
    }

    private function getGroups()
    {
        return Group::pluck('name', 'id');
    }

    public function getRoles()
    {
        return Role::pluck('name', 'id');
    }

    public function validatation($request)
    {
        return $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request['id'],
            'password' => 'nullable',
            'gender' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'institutions_code' => 'nullable',
            'role_id' => 'required',
        ]);
    }
}
