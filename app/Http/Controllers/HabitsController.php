<?php

namespace App\Http\Controllers;

use App\Http\Requests\HabitsRequest;
use App\Repositories\HabitsRepository;
use Illuminate\Http\Request;

class HabitsController extends Controller
{
    public function __construct(HabitsRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Habits';
        $this->request = HabitsRequest::class;
        $this->module = 'habits';
    }

    public function index()
    {
        $request = request();
        $data = $this->model->getItemByParent($request);
        $additional_url = '';
        if ($request->get('is_main_habits') === 'true') {
            $additional_url = '&is_main_habits=true';
        }

        return parent::index()->with([
            'items' => $data['detail'],
            'parent' => $data['parent'],
            'filter' => [
                'name' => 'text',
                'tipe' => 'select',
                'detail' => 'text',
            ],
            'tipe' => collect(['0' => 'Standar', '1' => 'Custom']),
            'custom_url_create' => 'parent='.$request->get('parent').$additional_url,
            'has_parent' => $request->get('parent'),
        ]);
    }

    public function redirectSuccess($actionFrom, Request $request, $result = null)
    {
        if ($actionFrom == 'delete') {
            return redirect()->back()->withMessage('Berhasil menghapus data');
        } else {
            $parent = $result['parent_id'];
            $is_main_habits = $result['is_main_habits'];
            if ($is_main_habits === 'true') {
                $url = 'parent='.$parent.'&is_main_habits=true';
            } else {
                $url = 'parent='.$parent;
            }

            return redirect('/habits?'.$url)->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
