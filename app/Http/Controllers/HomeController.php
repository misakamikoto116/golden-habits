<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Institution;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $user  = Auth::user();
        $role  = $user->roles->first()->name;
        $warna = [ 'success', 'navy', 'warning', 'info', 'danger', 'primary', 'purple', 'orange', 'maroon', 'teal', 'aqua' ];
        if (strtolower($role) == "admin instansi") {
            $institution = Institution::find($user->institution_id)->group->pluck('name', 'id');
            $conditions  = (count($institution->toArray()) > 0) ? " AND (group_id = " . implode(" OR `u`.`group_id` = ", array_keys($institution->toArray())) . ") " : "";
            $dataUsers   = DB::select("SELECT COUNT(*) AS `total`, IF(`g`.`id` IS NOT NULL, `g`.`id`, 0) AS `id` FROM `users` `u` LEFT JOIN `groups` `g` ON `u`.`group_id` = `g`.`id` WHERE `u`.`role_id` = 2 $conditions GROUP BY `g`.`id`");
            $data        = array_column($dataUsers, 'total', 'id');
            $jumlahUser  = array_sum($data);
        } elseif (strtolower($role) == "admin") {
            $institution = Institution::pluck('name', 'id');
            $dataUsers   = DB::select("SELECT COUNT(*) AS `total`, IF(`i`.`id` IS NOT NULL, `i`.`id`, 0) AS `id` FROM `users` `u` LEFT JOIN `groups` `g` ON `u`.`group_id` = `g`.`id` LEFT JOIN `institutions` `i` ON `g`.`institution_id` = `i`.`id` WHERE `u`.`role_id` = 2 GROUP BY `i`.`id`");
            $data        = array_column($dataUsers, 'total', 'id');
            $jumlahUser  = array_sum($data);
        }

        return view('home')->with(compact('data', 'institution', 'warna', 'jumlahUser'));
    }

    public function warna($key)
    {
        $warna = [
            'success',
            'navy',
            'warning',
            'info',
            'danger',
            'primary',
            'purple',
            'orange',
            'maroon',
            'teal',
            'aqua',
        ];
        if (!array_key_exists($key, $warna)) {
            $key = rand(0, 10);
        }

        return $warna[$key];
    }

    // public function UniqueRandomNumbersWithinRange($min, $max, $quantity)
    // {
    //     $numbers = range($min, $max);
    //     shuffle($numbers);

    //     return array_slice($numbers, 0, $quantity);
    // }
}
