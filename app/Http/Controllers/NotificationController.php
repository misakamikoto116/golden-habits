<?php

namespace App\Http\Controllers;

use App\Repositories\NotifcationRepository;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct(NotifcationRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Manajemen Notifikasi';
        $this->module = 'notification';
        $this->view = [
            'filter' => [
                'tanggal' => 'date_normal',
            ]
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function create()
    {
        $group['all'] = 'Semua Group';
        foreach ($this->model->getGroup() as $key => $value) {
            $group["$key"] = $value;
        }
        return parent::create()->with(compact('group'));
    }

}
