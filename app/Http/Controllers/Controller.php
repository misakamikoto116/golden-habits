<?php

namespace App\Http\Controllers;

use Generator\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    protected $viewNamespace = 'admin';
}
