<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupInstitutionRequest;
use App\Repositories\GroupInstitutionRepository;

class GroupInstitutionController extends Controller
{
    public function __construct(GroupInstitutionRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Manajemen Grup';
        $this->module = 'group_institution';
        $this->request = GroupInstitutionRequest::class;
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'kode_grup' => 'text',
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function redirectFail($actionFrom, \Illuminate\Http\Request $request, $exception = null)
    {
        Session::flash('type', 'danger');
        return redirect()->back()->withMessage($exception);
    }

}
