<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubMainHabitsRepository;
use App\Http\Requests\SubMainHabitsRequest;

class SubMainHabitsController extends Controller
{
    public function __construct(SubMainHabitsRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Sub Main Habits';
        $this->request = SubMainHabitsRequest::class;
        $this->module = 'sub_main_habits';
    }

    public function index()
    {
        $request = request();
        $data = $this->model->getItemByParent($request);

        return parent::index()->with([
            'items' => $data['detail'],
            'parent' => $data['parent'],
            'filter' => ['name' => 'text'],
            'custom_url_create' => 'parent='.$request->get('parent'),
            'has_parent' => $request->get('parent'),
        ]);
    }

    public function redirectSuccess($actionFrom, Request $request, $result = null)
    {
        if ($actionFrom == 'delete') {
            return redirect()->back()->withMessage('Berhasil menghapus data');
        } else {
            return redirect('/sub-main-habits?parent='.$result)->withMessage('Berhasil Menambah/Memperbarui data');
        }
    }
}
