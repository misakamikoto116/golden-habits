<?php

namespace App\Http\Controllers;

use App\Repositories\RoleRepository;
use App\Http\Requests\RoleRequest;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    public function __construct(RoleRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Manajemen Hak Alses';
        $this->request = RoleRequest::class;
        $this->module = 'role';
        $this->view = [
            'filter' => [
                'nama' => 'text',
            ],
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }

    public function redirectFail($actionFrom, \Illuminate\Http\Request $request, $exception = null)
    {
        Session::flash('type', 'danger');

        return redirect()->back()->withMessage('Tidak dapat menghapus roles yang memiliki user');
    }
}
