<?php

namespace App\Http\Controllers;

use App\Http\Requests\PengamalanHabitsUmumRequest;
use App\Repositories\PengamalanHabitsUmumRepository;

class PengamalanHabitsUmumController extends Controller
{
    public function __construct(PengamalanHabitsUmumRepository $repo)
    {
        $this->model = $repo;
        $this->title = 'Pengamalan Habits Umum';
        $this->request = PengamalanHabitsUmumRequest::class;
        $this->module = 'pengamalan_habits_umum';
        $this->view = [
            'filter' => [
                'nama' => 'text',
                'tanggal' => 'date',
                'habits' => 'text',
                'grup' => 'select',
            ],
            'create_unactive' => true,
            'grup' => $this->model->getGroup(),
        ];
    }

    public function index()
    {
        return parent::index()->with($this->view);
    }
}
