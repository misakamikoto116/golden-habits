<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstitusiUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (substr($this->url(), -6) == 'update') {
            $email    = "required|unique:users,email,$this->id";
            $password = "nullable";
        } else {
            $email    = "required|unique:users,email";
            $password = "required";
        }
        

        return [
            'name'     => 'required',
            'email'    => $email,
            'password' => $password,
        ];
    }
}
