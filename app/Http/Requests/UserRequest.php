<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')) {
            $validate = 'required';
            $unique = 'unique:users,email';
        } elseif ($this->isMethod('PUT')) {
            $validate = 'nullable';
            $unique = null;
        }

        $rules = [
            'name' => 'required',
            'email' => 'required|email|'.$unique,
            'password' => $validate,
            'gender' => 'required',
            'phone' => 'required',
            'group_id' => 'nullable',
            'image' => 'nullable',
            'role_id' => 'required',
        ];

        return $rules;
    }
}
