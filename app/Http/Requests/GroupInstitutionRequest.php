<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupInstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->isMethod('POST')) {
            $unique = 'unique:groups,group_code';
        } elseif ($this->isMethod('PUT')) {
            $unique = null;
        }

        $rules = [
            'name' => 'required',
            'group_code' => 'required|'.$unique,
        ];

        return $rules;
    }
}
