<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RealitationTadarusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_time' => 'required',
            'end_time' => 'required',
            'main_habits_id' => 'required',
            'date' => 'required',
            'place' => 'required',
            'juz' => 'required',
            'surah' => 'required',
            'isHatam' => 'boolean',
            'ayat' => 'required',
        ];
    }
}
