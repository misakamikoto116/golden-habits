<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AyyamulBidhRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_hijriyah' => 'required|numeric|max:30',
            'month_hijriyah' => 'required|numeric|max:12',
            'year_hijriyah' => 'required|numeric|max:9999',
            'date_masehi' => 'required|date',
        ];
    }
}
