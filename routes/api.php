<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('package-habits', 'Api\PackageHabitsController@index');
    Route::get('habits', 'Api\PackageHabitsController@habits');
    Route::get('package-habits-detail', 'Api\PackageHabitsController@detail');
    Route::get('package-habits-detail-with-habits', 'Api\PackageHabitsController@detailWithHabits');
    Route::get('score-main-habits', 'Api\PackageHabitsController@getScoreMainHabits');
    // Berkatian dengan user
    Route::post('save-user-habits', 'Api\PackageHabitsController@saveUserHabits');
    Route::post('post-group-code', 'Api\UserController@postGroupCode');
    Route::post('post-photo-profile', 'Api\UserController@postPhotoProfile');
    Route::post('update-profile', 'Api\UserController@postProfileUser');
    Route::post('update-password', 'Api\UserController@postChangePassword');
    Route::post('user/fcm-token', 'Api\UserController@postFcmToken');

    Route::post('post-realitation-basic', 'Api\RealitationController@postRealitationBasic');
    Route::delete('delete-realitation-basic', 'Api\RealitationController@deleteRelationBasic');
    Route::post('post-realitation-taklim', 'Api\RealitationController@postSubmitRealitationTaklim');
    Route::post('post-realitation-tadarus', 'Api\RealitationController@postSubmitRealitationTadarus');

    Route::get('get-realitation-tadarus-monthly', 'Api\RealitationController@getRealitationTadarusMonthly');
    Route::get('get-realitation-taklim-monthly', 'Api\RealitationController@getRealitationTaklimMonthly');

    // Narik2 data
    Route::get('get-main-habits', 'Api\HabitsController@getListMainHabits');

    Route::get('get-sub-main-habits', 'Api\HabitsController@getListSubMainHabits');
    Route::get('get-sub-main-habits-all', 'Api\HabitsController@getListSubMainHabitsAll');

    Route::get('get-habits', 'Api\HabitsController@getListHabits');
    Route::get('get-habits', 'Api\HabitsController@getListHabits');

    // Target Realitation
    Route::post('post-target-realitation', 'Api\TargetRealitationController@postTargetRealitation');

    // Shaum User Preference
    Route::post('post-shaum-user-preference', 'Api\ShaumUserPreferenceController@postShaumUserPreference');

    // Realitation Zis
    Route::post('post-realitation-zis', 'Api\RealitationZisController@postRealitationZis');
    Route::get('get-realitation-zis', 'Api\RealitationZisController@getRealitationZis');

    // Ayyamul Bidh
    Route::get('get-ayyamul-bidh', 'Api\AyyamulBidhController@getAyyamulBidh');

    // Score
    Route::get('myscore', 'Api\ScoreController@getMyScore');
    Route::get('myscore/detail/{bulan}/{tahun}', 'Api\ScoreController@getMyDetailScore');
});
