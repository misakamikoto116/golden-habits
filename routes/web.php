<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::put('/profile/update', 'ProfileController@update')->name('profile.update');
    Route::get('get/skor/habits', 'UsersPerfomanceController@getHabitsScore')->name('get.skor.habits');
});

Route::group(['middleware' => ['auth', 'role:admin']], function () {
    Route::resource('main-habits', 'MainHabitsController');
    Route::resource('sub-main-habits', 'SubMainHabitsController');
    Route::resource('habits', 'HabitsController')->except(['create']);
    Route::resource('lembaga', 'LembagaController');
    
    // add user institusi
    Route::get('lembaga/user/{id}', 'LembagaController@user')->name('lembaga.user');
    Route::post('lembaga/user/{id}/{action}', 'LembagaController@userstore')->name('lembaga.user.add');

    Route::resource('role', 'RoleController');
    Route::resource('group', 'GroupController');
    Route::resource('ayyamul-bidh', 'AyyamulBidhController');
    Route::resource('pengamalan-habits-umum', 'PengamalanHabitsUmumController');
    Route::resource('pengamalan-tadarus', 'RealitationTadarusController');
    Route::resource('pengamalan-zis', 'RealitationZisController');
});

Route::group(['middleware' => ['auth', 'role:admin instansi']], function () {
    Route::resource('group-institution', 'GroupInstitutionController');
});

Route::group(['middleware' => ['auth', 'role:admin|admin instansi']], function () {
    Route::resource('notification', 'NotificationController');
});

Route::group(['middleware' => ['auth', 'role:admin|user admin|admin instansi']], function () {
    Route::resource('user', 'UsersController');
    Route::resource('user-performance', 'UsersPerfomanceController');
    Route::get('userperformance/print', 'UsersPerfomanceController@print')->name('user-performance.print');
});
