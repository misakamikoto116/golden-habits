@if (session()->has('message'))
    <div class="alert alert-{{session()->get('type') ?? 'success'}} alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        {!! session()->get('message') !!}
    </div>
@endif

