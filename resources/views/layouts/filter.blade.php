<!--
    1. untuk mengisi select, maka buat index sesuai dengan nama $title nya.
    2. untuk custom jarak column bisa pakai index 'jarak'
    3. untuk custom format date, maka buat index sesuai dengan nama $title nya.
    NB : itu semua diluar index 'filter'
-->
@if(isset($filter))
{{ Form::open(['url' => url()->current(), 'method' => 'GET', $custom_url_create ?? null]) }}
    {{ Form::hidden('search', true) }}
    @if(isset($has_parent))
        {{ Form::hidden('parent', $has_parent) }}
    @endif
        <div class="form-group row">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12" >
                @foreach($filter as $title => $type)
                    @include('layouts.filter_component')
                    @php break; @endphp
                @endforeach
            </div>
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <button class=" btn btn-success"><i class="fa fa-search"></i> Cari</button>
                @if(count($filter) > 1)
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalFilter"> <i class="fa fa-filter"></i> Advance Search</button>
                @endif
                @if(Request::get('search'))
                    @if(isset($custom_url_create))
                        <a href="{{ url()->current().'/?'.$custom_url_create }}" class="btn btn-danger"><i class="fa fa-close"> Clear</i></a>
                    @else 
                        <a href="{{ url()->current() }}" class="btn btn-danger"><i class="fa fa-close"> Clear</i></a>
                    @endif
                @endif
                @if(isset($create_unactive))
                @else
                <a href="{{ route($module_url->create, $custom_url_create ?? null) }}" class="btn btn-primary pull-right" dusk="btn-tambah-data">Tambah Data</a>
                @endif
            </div>
            
        </div>
{{ Form::close() }}

<div class="modal fade" id="modalFilter" role="dialog" aria-labelledby="modalFilter" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalFilter">Pencarian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['url' => url()->current(), 'method' => 'GET']) !!}
      <div class="modal-body">
        {{ Form::hidden('search', true) }}
        @if(isset($has_parent))
            {{ Form::hidden('parent', $has_parent) }}
        @endif
            @foreach($filter as $title => $type)
                <div class="form-group">
                    <label for="$title" class="col-form-label"> {{ str_replace('_',' ', ucfirst($title)) }} :</label>    
                    @include('layouts.filter_component')
                </div>
            @endforeach
      </div>
      <div class="modal-footer">
        <button class=" btn btn-success"><i class="fa fa-search"></i> Cari</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
      {!! Form::close() !!}

    </div>
  </div>
</div>

@section('scripts')
    <script src="{{ asset('plugins/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
    <script>
        @foreach($filter as $title => $type)
            @if($type == 'select')
                $('.{{ $title }}').select2({
                    width: '100%',
                    allowClear: true,
                    placeholder: 'Pencarian  {{ str_replace("_", " ", ucfirst($title)) }}'
                });
            @endif
            @if($type == 'select-ajax')
                $('.{{ $title }}').select2({
                    allowClear: false,
                    width: '100%',
                    placeholder: 'Pencarian  {{ str_replace("_", " ", ucfirst($title)) }}',
                    ajax: {
                        url: '{{ $$title }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            console.log(params);
                            return {
                                search: params.term, // search term
                                page: params.page
                            };
                        },
                        cache: true
                    }
                });
            @endif
            @if($type == 'date')
                $('.{{ $title }}').datetimepicker({
                    format: '{{ $$title ?? "DD-MM-YYYY" }}'
                });
            @endif

            @if($type == 'date_interval')
                $('.{{ $title."_start" }}').datetimepicker({
                    format: '{{ $title ?? "DD-MM-YYYY" }}',
        			// date: new Date({{ date('Y') }}, {{ date('m') - 1 }}, {{ date('d') }})
                });
                    // var start_date = $('.{{ $title."_start" }}').val();
                    // var array_date = start_date.split('-');
                    // alert(array_date[0])
                    $('.{{ $title."_end" }}').datetimepicker({
                        format: '{{ $title ?? "DD-MM-YYYY" }}',
                        // minDate: new Date(array_date[2], array_date[1] - 1, array_date[0]),
                        // disabledDates: [new Date(array_date[2], array_date[1] - 1, array_date[0])]
                    });

            @endif

        @endforeach
    </script>
@endsection
@endif
