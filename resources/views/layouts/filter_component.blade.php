
@if($type == 'text')
    {!! Form::text($title, Request::get($title), ['class' => 'form-control '.$title, 'id' => $title, 'placeholder' => 'Pencarian '.str_replace('_',' ', ucfirst($title))]) !!}
@endif

@if($type == 'select')
    {{ Form::select($title, $$title, Request::get($title), ['id' => $title, 'class' => 'form-control '.$title, 'placeholder' => 'Pencarian']) }}
@endif

@if($type == 'select-ajax')
    {{ Form::select($title, [], Request::get($title), ['id' => $title, 'class' => 'form-control '.$title, 'placeholder' => 'Pencarian']) }}
@endif

@if($type == 'date')
    {!! Form::text($title, Request::get($title), ['class' => 'form-control '.$title, 'id' => $title, 'placeholder' => 'Pencarian '.str_replace('_',' ', ucfirst($title))]) !!}
@endif

@if($type == 'date_normal')
    <input type="date" name="{{ $title }}" id="{{ $title }}" class="form-control" value="{{ Request::get($title) }}">
@endif

@if($type == 'month')
    <input type="month" name="{{ $title }}" id="{{ $title }}" class="form-control" value="{{ Request::get($title) }}">
@endif

@if($type == 'date_interval')
    {!! Form::text($title.'_start', Request::get($title.'_start'), ['class' => 'form-control '.$title, 'id' => $title.'_start', 'placeholder' => 'Dari tanggal']) !!}
    <br>
    {!! Form::text($title.'_end', Request::get($title.'_end'), ['class' => 'form-control '.$title, 'id' => $title.'_end', 'placeholder' => 'Sampai tanggal']) !!}
@endif

@if($type == 'amount')
    {!! Form::text($title, Request::get($title), ['class' => 'form-control amount '.$title, 'id' => $title, 'placeholder' => 'Pencarian '.str_replace('_',' ', ucfirst($title))]) !!}
@endif

@if($type == 'number')
    {!! Form::number($title, Request::get($title), ['class' => 'form-control '.$title, 'id' => $title, 'placeholder' => 'Pencarian '.str_replace('_',' ', ucfirst($title))]) !!}
@endif