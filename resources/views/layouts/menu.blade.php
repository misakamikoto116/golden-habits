<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">NAVIGATION</li>
    <li class="{{ Request::segment(1) == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    @hasanyrole('admin')
    <li class="{{ Request::segment(1) == 'main-habits' ? 'active' : '' }}"><a href="{{ url('main-habits') }}"><i class="fa fa-book"></i> Habits</a></li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-bookmark"></i> <span>Pengamalan Habits</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
        <li {{ Request::segment(1) == 'pengamalan-habits-umum' ? 'active' : '' }}><a href="{{ url('pengamalan-habits-umum') }}"><i class="fa fa-circle-o"></i> Pengamalan Habits Umum</a></li>
            <li><a href="{{ url('pengamalan-zis') }}"><i class="fa fa-circle-o"></i> Pengamalan ZIS</a></li>
            <li><a href="{{ url('pengamalan-tadarus') }}"><i class="fa fa-circle-o"></i> Pengamalan Tadarus Al-Quran</a></li>
        </ul>
    </li>
    <li class="{{ Request::segment(1) == 'lembaga' ? 'active' : '' }}"><a href="{{ url('lembaga') }}"><i class="fa fa-gears"></i> Manajemen Lembaga</a></li>
    @endhasanyrole()
    @hasanyrole('user admin|admin|admin instansi')
    <li class="{{ Request::segment(1) == 'user' ? 'active' : '' }}"><a href="{{ url('user') }}"><i class="fa fa-user"></i> Manajemen User</a></li>
    <li class="{{ Request::segment(1) == 'user-performance' ? 'active' : '' }}"><a href="{{ url('user-performance') }}"><i class="fa fa-universal-access"></i> Performa User</a></li>
    @endhasanyrole()
    @hasanyrole('admin')
    <li class="{{ Request::segment(1) == 'ayyamul-bidh' ? 'active' : '' }}"><a href="{{ url('ayyamul-bidh') }}"><i class="fa fa-calendar"></i> Ayyamul Bidh</a></li>
    <li class="{{ Request::segment(1) == 'role' ? 'active' : '' }}"><a href="{{ url('role') }}"><i class="fa fa-key"></i> Manajemen Role</a></li>
    <li class="{{ Request::segment(1) == 'notification' ? 'active' : '' }}"><a href="{{ url('notification') }}"><i class="fa fa-bell"></i> Manajemen Notifikasi</a></li>
    @endhasanyrole()

    @hasanyrole('admin instansi')
    <li class="{{ Request::segment(1) == 'group-institution' ? 'active' : '' }}"><a href="{{ url('group-institution') }}"><i class="fa fa-group"></i> Manajemen Group</a></li>
    <li class="{{ Request::segment(1) == 'notification' ? 'active' : '' }}"><a href="{{ url('notification') }}"><i class="	fa fa-comments-o"></i> Manajemen Notifikasi</a></li>

    @endhasanyrole()
</ul>