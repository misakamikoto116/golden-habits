@extends('layouts.app')

@section('content')

@if (session('success'))
<div class="callout callout-success">
    <p>{{ session('success') }}</p>
</div>
@endif

@if ($errors->any())
<div class="callout callout-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ (is_null($user)) ? 'Add' : 'Update' }} User Instansi</h3>
    </div>
    {{ Form::model($user, ['route' => ['lembaga.user.add', $id, $action], 'method' => 'POST']) }}
        <div class="box-body">
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nama</label>
                {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'User Name', 'value'=> (is_null($user)) ? '' : $user->name, 'required' => true]) }}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                {{ Form::email('email', old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'User Email', 'value'=> (is_null($user)) ? '' : $user->email, 'required' => true]) }}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            @if (is_null($user))
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Password</label>
                {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password', 'value'=> (is_null($user)) ? '' : $user->password, 'required' => true]) }}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
            @else
            <input type="hidden" name="id" value="{{ $user->id }}">
            @endif
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right" dusk="btn-simpan">Simpan</button>
        </div>
    {{ Form::close() }}
</div>

@endsection