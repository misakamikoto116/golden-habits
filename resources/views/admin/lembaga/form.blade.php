
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
	<label for="address">Alamat</label>
	{{ Form::textarea('address', old('address'), ['class' => 'form-control', 'id' => 'address', 'placeholder' => 'Alamat lembaga', 'required']) }}
	{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
	
<div class="form-group {{ $errors->has('kota') ? ' has-error' : '' }}">
	<label for="kota">Kota</label>
	{{ Form::text('kota', old('kota'), ['class' => 'form-control', 'id' => 'kota', 'placeholder' => 'Kota', 'required']) }}
	{!! $errors->first('kota', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('provinsi') ? ' has-error' : '' }}">
	<label for="provinsi">Provinsi</label>
	{{ Form::text('provinsi', old('provinsi'), ['class' => 'form-control', 'id' => 'provinsi', 'placeholder' => 'Provinsi', 'required']) }}
	{!! $errors->first('provinsi', '<p class="help-block">:message</p>') !!}
</div>