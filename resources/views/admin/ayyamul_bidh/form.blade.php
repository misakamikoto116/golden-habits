
<div class="form-group {{ $errors->has('date_hijriyah') ? ' has-error' : '' }}">
	<label for="date_hijriyah">Tanggal Hijryiah</label>
	{{ Form::number('date_hijriyah', old('date_hijriyah'), ['class' => 'form-control', 'id' => 'date_hijriyah', 'min' => 1, 'max' => 31, 'placeholder' => 'Tanggal (angka)', 'required']) }}
	{!! $errors->first('date_hijriyah', '<p class="help-block">:message</p>') !!}
</div>
	
<div class="form-group {{ $errors->has('month_hijriyah') ? ' has-error' : '' }}">
	<label for="month_hijriyah">Bulan Hijriyah</label>
	{{ Form::number('month_hijriyah', old('month_hijriyah'), ['class' => 'form-control', 'id' => 'month_hijriyah', 'min' => 1, 'max' => 12, 'placeholder' => 'Bulan (angka)', 'required']) }}
	{!! $errors->first('month_hijriyah', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('year_hijriyah') ? ' has-error' : '' }}">
	<label for="year_hijriyah">Tahun Hijriyah</label>
	{{ Form::number('year_hijriyah', old('year_hijriyah'), ['class' => 'form-control', 'id' => 'year_hijriyah', 'placeholder' => 'Tahun', 'required']) }}
	{!! $errors->first('year_hijriyah', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('date_masehi') ? ' has-error' : '' }}">
	<label for="date_masehi">Tanggal & Tahun Masehi</label>
	{{ Form::text('date_masehi', old('date_masehi'), ['class' => 'form-control datetime', 'id' => 'date_masehi', 'placeholder' => 'Tahun Masehi', 'required']) }}
	{!! $errors->first('date_masehi', '<p class="help-block">:message</p>') !!}
</div>

@section('js')
	<script>
		$('.datetime').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	</script>
@endsection