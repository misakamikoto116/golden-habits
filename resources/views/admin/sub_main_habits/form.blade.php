<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	{{ Form::hidden('main_habits_id', Request::get('parent') ?? null) }}
</div>
