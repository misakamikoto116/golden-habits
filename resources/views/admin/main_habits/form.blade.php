<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('is_have_sub_main_habits') ? 'has-error' : '' }}">
	<label for="is_have_sub_main_habits">
		{{ Form::checkbox('is_have_sub_main_habits', 1, null, ['id' => 'is_have_sub_main_habits']) }}
		Memiliki Sub Main Habits</label>

</div>