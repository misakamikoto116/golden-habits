@extends('layouts.app')

@section('content')
@include('layouts.filter')
{{-- {!! session() !!} --}}
<div class="clearfix"></div>
<br>
<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">{{ $title_document }}</h3>

        {{-- <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
            <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div> --}}
    </div>
    <div class="box-body">
        @if ($items->isEmpty())
        <div class="alert alert-warning alert-dismissible" role="alert">
            <strong>Informasi</strong>

            Tidak Ada Data
        </div>
    @else
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th>Date</th>
                        <th>Success</th>
                        {{-- <th width="15%">Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->message }}</td>
                            <td>{{ $item->date }}</td>
                            <td>{!! $item->success_formatted !!}</td>
                            {{-- <td>
                                <a href="#" data-toggle="tooltip" title="Send Again" class="btn btn-success"> <i class="fa fa-refresh"></i></a>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div>
            {{ $items->appends(Request::all())->links() }}
        </div>        
    </div>
    <!-- /.box-footer-->
</div>


@endsection

@section('script')
    <script src="{{ asset('js/laravel.js') }}"></script>
@endsection