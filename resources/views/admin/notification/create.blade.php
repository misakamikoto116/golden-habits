@extends('layouts.app')

@section('content')

		<!-- Horizontal Form -->
		<div class="box box-info">
		  <div class="box-header with-border">
			<h3 class="box-title">{{ $title_document }}</h3>
		  </div>
		  <!-- /.box-header -->
		  <!-- form start -->
		{{ Form::open(['route' => $module_url->store, 'method' => 'POST','files' => true]) }}
			<div class="box-body">
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Title</label>
					{{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title Notification', 'required']) }}
					{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Group</label>
					{{ Form::select('group', $group, null, ['class' => 'form-control', 'id' => 'title', 'required']) }}
					{!! $errors->first('group', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Text Message</label>
					{{ Form::textarea('message', old('message'), ['class' => 'form-control', 'id' => 'message', 'rows' => 3, 'placeholder' => 'Text Message Notification ..', 'required']) }}
					{!! $errors->first('message', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary pull-right" dusk="btn-simpan">Simpan dan Kirim Notifikasi</button>
			</div>
		{{ Form::close() }}
		</div>

@endsection

@section('js')
@endsection
