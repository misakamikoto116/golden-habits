@extends('layouts.app')

@section('content')
@include('layouts.filter')
{{-- {!! session() !!} --}}
<div class="clearfix"></div>
<br>
<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">{{ $title_document }}</h3>
    </div>
    <div class="box-body">
        @if ($items->isEmpty())
        <div class="alert alert-warning alert-dismissible" role="alert">
            <strong>Informasi</strong>

            Tidak Ada Data
        </div>
    @else
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th>Nama User</th>
                        <th>Hari & Tanggal</th>                        
                        <th>Jam Mulai</th>
                        <th>Jam Selesai</th>
                        <th>Durasi</th>
                        <th>Juz ke</th>
                        <th>Surah</th>
                        <th>Ayat</th>
                        <th>Tempat</th>
                        <th>Group</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->tanggal_formatted }}</td>
                            <td>{{ $item->start_time }}</td>
                            <td>{{ $item->end_time }}</td>
                            <td>{{ $item->duration }}</td>
                            <td>{{ $item->juz }}</td>
                            <td>{{ $item->surah }}</td>
                            <td>{{ $item->ayat }}</td>
                            <td>{{ $item->place }}</td>
                            <td>{{ $item->user->group ? $item->user->group->name : '-' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div>
            {{ $items->appends(Request::all())->links() }}
        </div>        
    </div>
    <!-- /.box-footer-->
</div>


@endsection

@section('script')
    <script src="{{ asset('js/laravel.js') }}"></script>
@endsection