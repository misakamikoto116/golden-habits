@extends('layouts.app')
@section('css')
    <style>
    .thumbnail{
        border-radius: 5px;
        width: 100px;
        height: 100px;  
    }

    .image-modal{
        padding: 10px;
        max-width: 700px;
        max-height: 700px;
        border-radius: 5px;
    }
    .close-modal{
        font-size: 60px;
        float: right;
        background: transparent;
        border: none;
    }
    </style>
@endsection
@section('content')
@include('layouts.filter')
{{-- {!! session() !!} --}}
<div class="clearfix"></div>
<br>
<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">{{ $title_document }}
            <br>
            <small style="font-size:16px;">{{ ucfirst($parent->name) }}</small>
        </h3>

    </div>
    <div class="box-body">
        @if ($items->isEmpty())
        <div class="alert alert-warning alert-dismissible" role="alert">
            <strong>Informasi</strong>

            Tidak Ada Data
        </div>
    @else
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th>Nama</th>
                        <th>Tipe</th>
                        <th>Detail</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->tipe }}</td>
                            <td>{{ $item->periode_trigger }}</td>
                            <td>

                                <a href="{{ route($module_url->edit, $item->id) }}" class="btn btn-warning waves-effect waves-light" dusk="btn-option-edit-{{ $item->id }}"> <i class="fa fa-pencil-square-o"></i></a>
                                
                                <a class="btn btn-danger waves-effect waves-light" data-dismiss="modal" onclick="$('#delete-{{ $item->id }}').modal('show')" dusk="hapus-{{ $item->id }}"><i class="fa fa-trash"></i></a>

                                <div class="modal fade" role="dialog" tabindex="-1" id="delete-{{ $item->id }}">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Perhatian!</h4>
                                            </div>
                                            <div class="modal-body">
                                                Hapus data, Yakin ?

                                                <div class="row" style="margin-top:50px;">
                                                    <div class="col-md-6">
                                                        <a href="{{ route('habits.destroy', $item->id) }}" data-method="DELETE" class="btn btn-danger" dusk="yesd-{{ $item->id }}">Ya, hapus!</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a class="btn btn-inverse" data-dismiss="modal" onclick="$('#option-{{ $item->id }}').modal('show')">Batalkan</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @endif
    <!-- /.box-footer-->
</div>


@endsection

@section('script')
    <script src="{{ asset('js/laravel.js') }}"></script>
@endsection