<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	{{ Form::hidden('is_main_habits', Request::get('is_main_habits') ?? null) }}
	{{ Form::hidden('parent_id', Request::get('parent') ?? null) }}
</div>

<div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
	<label for="type">Tipe</label>
	{{ Form::select('type',['0' => 'Basic', '1' => 'Custom'], old('type'), ['class' => 'form-control', 'id' => 'type', 'placeholder' => 'Tipe', 'required']) }}
	{!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('periode') ? ' has-error' : '' }}">
	<label for="periode_trigger">Periode Trigger</label>
	{{ Form::text('periode_trigger', old('periode_trigger'), ['class' => 'form-control', 'id' => 'periode_trigger', 'placeholder' => 'Periode Trigger', 'required']) }}
	{!! $errors->first('periode_trigger', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('isFardhu') ? ' has-error' : '' }}">
	<div class="checkbox">
		<label style="font-size:18px; font-weight:bold;">
			{{ Form::checkbox('isFardhu', 1, null, ['style' => 'margin-top:5px;']) }}
			Fardhu
		</label>
	  </div>
</div>

@section('js')
@endsection