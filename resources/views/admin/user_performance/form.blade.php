<div class="form-group {{ $errors->has('main_habits_id') ? ' has-error' : '' }}">
	<label for="main_habits_id">Main Habits</label>
	{{ Form::select('main_habits_id', $main_habits, old('main_habits_id'), ['class' => 'form-control', 'id' => 'main_habits_id', 'placeholder' => 'Pilih Main Habits', 'required']) }}
	{!! $errors->first('main_habits_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('group_id') ? ' has-error' : '' }}">
	<label for="group_id">Grup</label>
	{{ Form::select('group_id', $groups, old('group_id'), ['class' => 'form-control', 'id' => 'group_id', 'placeholder' => 'Pilih Grup', 'required']) }}
	{!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
	<label for="date">Tanggal</label>
	{{ Form::text('date', old('date'), ['class' => 'form-control datetime', 'id' => 'date', 'placeholder' => 'Tanggal', 'required']) }}
	{!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>

@section('js')
	<script>
		
		$('.datetime').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	</script>
@endsection