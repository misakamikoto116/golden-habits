<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Print User Performence</title>
    <style>
        @media print{
            @page {
                size: landscape;
            }
        }
        body {
            margin-top: 5mm;
            margin-bottom: 5mm;
            margin-left: 5mm;
            margin-right: 5mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .table-bordered {
            width: 100%;
            border-collapse: collapse;
        }

        .table-bordered td {
            border: 1px solid black;
            vertical-align: middle;
            padding: 3px;
        }
    </style>
</head>
<body>
    @php($month = request()->bulan ?? date('m'))
    <table width="100%">
        <tr>
            <td align="center">
                <h4><b>USER PERFORMENCE</b></h4>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td width="30">Bulan</td>
                        <td>: {{ date('F Y', strtotime(date('Y-' . $month))) }}</td>
                        <td width="30">User</td>
                        <td width="110">: {{ Auth::user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Group</td>
                        <td>: {{ HelperView::group(request()->group) }}</td>
                        <td>Print</td>
                        <td>: {{ date('d F Y') }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table-bordered">
                    <thead>
                        <tr>
                            <td align="center" rowspan="2" width="25">
                                <b>NO</b>
                            </td>
                            <td align="center" rowspan="2" width="150">
                                <b>NAMA</b>
                            </td>
                            <td align="center" colspan="{{ count($mainHabits) }}">
                                <b>MAIN HABITS</b>
                            </td>
                        </tr>
                        <tr>
                            @foreach ($mainHabits as $habits)
                            <td align="center">
                                <b>{{ strtoupper($habits) }}</b>
                            </td>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        @php($score = $item->getScoreHabits($month))
                        <tr>
                            <td align="center">{{ $loop->iteration }}</td>
                            <td>{{ $item->name ?? null}}{{ $item->id}}</td>
                            @foreach ($mainHabits as $idHabits => $value)
                            <td align="center">
                                @if (isset($score[$idHabits]))
                                    {{ number_format($score[$idHabits], 2, ',', '') }}
                                @endif
                            </td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>