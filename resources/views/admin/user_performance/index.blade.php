@extends('layouts.app')

@section('content')
@include('layouts.filter')
{{-- {!! session() !!} --}}
<div class="clearfix"></div>
<br>
<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">{{ $title_document }}</h3>
        <div class="box-tools pull-right">
            <a href="{{ route('user-performance.print', [
                'name' => request()->name,
                'group' => request()->group,
                'bulan' => request()->bulan,
            ]) }}" target="_blank" class="btn bg-blue btn-sm">
                <i class="fa fa-print"></i> Print
            </a>
        </div>
    </div>
    <div class="box-body">
        @if ($items->isEmpty())
        <div class="alert alert-warning alert-dismissible " role="alert">
            <strong>Informasi</strong>
            
            Tidak Ada Data
        </div>
        @else
            @php($month = Request::get('bulan') ?? date('m'))
            <div class="table-responsive">
                <table class="table table-bordered" style="width:2000px">
                    <thead class="bg-blue">
                        <tr>
                            <td rowspan="2"  style="width:30px;vertical-align:middle" align="center"><b>NO</b></td>
                            <td rowspan="2"  style="width:180px;vertical-align:middle" align="center"><b>NAMA</b></td>
                            <td colspan="{{ count($mainHabits) }}" style="width:150px;vertical-align:middle" align="center"><b>MAIN HABITS</b></td>
                        </tr>
                        <tr>
                            @foreach ($mainHabits as $habits)
                                <td style="width:150px;vertical-align:middle" align="center">
                                    <b>{{ strtoupper($habits) }}</b>
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        @php($score = $item->getScoreHabits($month))
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name ?? null}}{{ $item->id}}</td>
                            @foreach ($mainHabits as $idHabits => $value)
                            <td style="width:150px;vertical-align:middle" align="center">
                                @if (isset($score[$idHabits]))
                                <span class="label bg-green">
                                    {{ number_format($score[$idHabits], 2, ',', '') }}
                                </span>
                                @endif
                            </td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div>
            {{ $items->appends(Request::all())->links() }}
        </div>        
    </div>
    <!-- /.box-footer-->
@endsection

@section('script')
    <script src="{{ asset('js/laravel.js') }}"></script>
    <script>
        let openDetail = (id) => {
            $.ajax({
                'method': 'get',
                'url': '{{ route("get.skor.habits") }}',
                'data': 'id='+id
                'success': function(response){
                    
                }
            });
        }
    </script>
@endsection