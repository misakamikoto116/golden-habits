@extends('layouts.app')

@section('content')

		<!-- Horizontal Form -->
		<div class="box box-info">
		  <div class="box-header with-border">
			<h3 class="box-title">{{ $title_document }}</h3>
		  </div>
		  <!-- /.box-header -->
		  <!-- form start -->
		{{ Form::open(['route' => $module_url->store, 'method' => 'POST','files' => true]) }}
			<div class="box-body">
				@include($form)
				<hr>
				<h4>Insert User as Admin Group</h4>

				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Nama</label>
					{{ Form::text('user_name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'User Name']) }}
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>

				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email">Email</label>
					{{ Form::email('user_email', old('user_email'), ['class' => 'form-control', 'id' => 'user_email', 'placeholder' => 'User Email']) }}
					{!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}
				</div>

				<div class="form-group {{ $errors->has('user_password') ? ' has-error' : '' }}">
					<label for="user_password">Password</label>
					{{ Form::password('user_password', ['class' => 'form-control', 'id' => 'user_password', 'placeholder' => 'Password']) }}
					{!! $errors->first('user_password', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary pull-right" dusk="btn-simpan">Simpan</button>
			</div>
			<!-- /.box-footer -->
		{{ Form::close() }}
		</div>

@endsection

