
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('group_code') ? ' has-error' : '' }}">
	<label for="group_code">Kode</label>
	{{ Form::text('group_code', old('group_code'), ['class' => 'form-control', 'id' => 'group_code', 'placeholder' => 'Kode Grup', 'required']) }}
	{!! $errors->first('group_code', '<p class="help-block">:message</p>') !!}
</div>

@section('js')
	<script type="text/javascript">
	$('document').ready(function(){
		$("#group_code").on({
		keydown: function(e) {
			if (e.which === 32)
			return false;
		},
		change: function() {
			this.value = this.value.replace(/\s/g, "");
		}
		});

		$('#group_code').on('change', function(){
			this.value = this.value.toLowerCase();
		});
	});
	</script>
@endsection