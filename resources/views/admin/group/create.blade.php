@extends('layouts.app')

@section('css')
<style>
	.user-group-add {
		background-color: #00c0ef;
		color: #fff;
		margin: 10px -10px;
		padding: 10px;
		text-align: center;
		text-transform: uppercase;
		font-weight: 600;
		letter-spacing: 1px;
	}
</style>
@endsection

@section('content')

		<!-- Horizontal Form -->
		<div class="box box-info">
		  <div class="box-header with-border">
			<h3 class="box-title">{{ $title_document }}</h3>
		  </div>
		{{ Form::open(['route' => $module_url->store, 'method' => 'POST','files' => true]) }}
			<div class="box-body">
				@include($form)
				<div class="checkbox">
					<label>
						<input type="checkbox" name="add_user_group"> Created User Group
					</label>
				</div>
				<div id="group-user-add" style="display:none">
					<h5 class="user-group-add">Add User as Group Admin</h5>  
					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Nama</label>
						{{ Form::text('user_name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'User Name']) }}
						{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
					</div>
	
					<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">Email</label>
						{{ Form::email('user_email', old('user_email'), ['class' => 'form-control', 'id' => 'user_email', 'placeholder' => 'User Email']) }}
						{!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}
					</div>
	
					<div class="form-group {{ $errors->has('user_password') ? ' has-error' : '' }}">
						<label for="user_password">Password</label>
						{{ Form::password('user_password', ['class' => 'form-control', 'id' => 'user_password', 'placeholder' => 'Password']) }}
						{!! $errors->first('user_password', '<p class="help-block">:message</p>') !!}
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary pull-right" dusk="btn-simpan">Simpan</button>
			</div>
			<!-- /.box-footer -->
		{{ Form::close() }}
		</div>

@endsection

@section('js')
<script>
$('input[type="checkbox"][name="add_user_group"]').change(function() {
     if(this.checked) {
		 $('#group-user-add').stop().fadeIn(200)
		 $('#group-user-add').find('input').prop('required', true)
     } else {
		 $('#group-user-add').stop().fadeOut(200)
		 $('#group-user-add').find('input').prop('required', false)
	 }
})
</script>

@endsection
