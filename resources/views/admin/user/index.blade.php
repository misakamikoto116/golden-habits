@extends('layouts.app')

@section('content')
@include('layouts.filter')
{{-- {!! session() !!} --}}
<div class="clearfix"></div>
<br>
<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">{{ $title_document }}</h3>

        {{-- <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
            <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div> --}}
    </div>
    <div class="box-body">
        @if ($items->isEmpty())
        <div class="alert alert-warning alert-dismissible " role="alert">
            <strong>Informasi</strong>

            Tidak Ada Data
        </div>
    @else
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Grup</th>
                        <th>Role</th>
                        <th width="15%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name ?? null}}</td>
                            <td>{{ $item->email ?? null }}</td>
                            <td>{{ $item->gender ?? null }}</td>
                            <td>{{ $item->phone ?? null }}</td>
                            <td>{{ $item->group->name ?? '-' }}</td>
                            <td>{{ $item->roles->first()->name }}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#target{{ $item->id }}" class="btn btn-info waves-effect waves-light" > <i class="fa fa-eye"></i></a>
                                <a href="{{ route($module_url->edit, $item->id) }}" class="btn btn-warning waves-effect waves-light" dusk="btn-option-edit-{{ $item->id }}"> <i class="fa fa-pencil-square-o"></i></a>
                                
                                <a class="btn btn-danger waves-effect waves-light" data-dismiss="modal" onclick="$('#delete-{{ $item->id }}').modal('show')" dusk="hapus-{{ $item->id }}"><i class="fa fa-trash"></i></a>

                                <div class="modal fade" role="dialog" tabindex="-1" id="delete-{{ $item->id }}">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Perhatian!</h4>
                                            </div>
                                            <div class="modal-body">
                                                Hapus data, Yakin ?

                                                <div class="row" style="margin-top:50px;">
                                                    <div class="col-md-6">
                                                        <a href="{{ route('user.destroy', $item->id) }}" data-method="DELETE" class="btn btn-danger" dusk="yesd-{{ $item->id }}">Ya, hapus!</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a class="btn btn-inverse" data-dismiss="modal" onclick="$('#option-{{ $item->id }}').modal('show')">Batalkan</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="target{{ $item->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                  
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Target Realisasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-hover table-striped">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Target</th>
                                                    <th>Main Habits</th>
                                                    <th>Periode</th>
                                                    <th>Hasil</th>
                                                </tr>
                                                @foreach($item->realizationTarget as $target)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $target->target }}</td>
                                                        <td>{{ $target->mainHabits->name }}</td>
                                                        <td>{{ $target->priod }}</td>
                                                        <td>{{ $target->result ?? '-' }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                  
                                    </div>
                                  </div>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    @endif
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div>
            {{ $items->appends(Request::all())->links() }}
        </div>        
    </div>
    <!-- /.box-footer-->
</div>


@endsection

@section('script')
    <script src="{{ asset('js/laravel.js') }}"></script>
@endsection