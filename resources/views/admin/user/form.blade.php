
@section('css')
<style>
	.warning-pass{
		color: red;
	}
</style>
	
@endsection
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name">Nama</label>
	{{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama', 'required']) }}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
	<label for="email">Email</label>
	{{ Form::email('email', old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email', 'required']) }}
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
	<label for="gender">Jenis Kelamin</label>
	{{ Form::select('gender', ['Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'], $item->gender ?? old('gender'), ['class' => 'form-control select2', 'id' => 'gender', 'placeholder' => 'Jenis Kelamin', 'required']) }}
	{!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
	<label for="phone">No. Telepon</label>
	{{ Form::text('phone', old('phone'), ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Telepon', 'required']) }}
	{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('group_id') ? ' has-error' : '' }}">
	<label for="group_id">Grup</label>
	{{ Form::select('group_id', $groups, old('group_id'), ['class' => 'form-control select2', 'id' => 'group_id', 'placeholder' => 'Grup']) }}
	{!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('shaum_daud_position') ? ' has-error' : '' }}">
	<label for="shaum_daud_position">Puasa Daud</label>
	{{ Form::select('shaum_daud_position', ['0' => 'Tidak Puasa', '1' => 'Puasa'], old('shaum_daud_position'), ['class' => 'form-control select2', 'id' => 'shaum_daud_position', 'placeholder' => 'Puasa Daud']) }}
	{!! $errors->first('shaum_daud_position', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
	<label for="password">Password</label>
	{{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password']) }}
	{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
	<p class="help-block warning-pass">Password tidak cocok</p>
</div>

<div class="form-group {{ $errors->has('password2') ? ' has-error' : '' }}">
	<label for="password2">Ulangi Password2</label>
	{{ Form::password('password2', ['class' => 'form-control', 'id' => 'password2', 'placeholder' => 'Konfirmasi Password']) }}
	{!! $errors->first('password2', '<p class="help-block">:message</p>') !!}
</div>	

<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
	<label for="image">Photo Profile</label>
	{{ Form::file('image', ['class' => 'form-control', 'id' => 'image']) }}
	{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
	<label for="role_id">Peran/Roles/Hak Akses</label>
	{{ Form::select('role_id', $roles, $item->roles ?? old('role_id'), ['class' => 'form-control select2', 'id' => 'role_id', 'placeholder' => 'Hak Akses', 'required']) }}
	{!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
</div>


@section('js')
<script>

$(function(){
    $('.warning-pass').hide();
    $('#password').on('keyup', function () {
        let pass2 = $('#password2').val();
        if ($(this).val() != pass2) {
            $('.btn-simpan').prop('disabled', true);
            $('.warning-pass').show(400);
        } else {
            $('.btn-simpan').prop('disabled', false);
            $('.warning-pass').hide(400);
        }
    });

    $('#password2').on('keyup', function () {
        let pass = $('#password').val();
        console.log(pass);
        if ($(this).val() != pass) {
            $('.btn-simpan').prop('disabled', true);
            $('.warning-pass').show(400);
        } else {
            $('.btn-simpan').prop('disabled', false);
            $('.warning-pass').hide(400);
        }
    });
});
</script>
	
@endsection