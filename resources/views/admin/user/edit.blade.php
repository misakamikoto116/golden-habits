
@extends('layouts.app')

@section('content')

		<!-- Horizontal Form -->
		<div class="box box-info">
		  <div class="box-header with-border">
			<h3 class="box-title">{{ $title_document }}</h3>
		  </div>
		  <!-- /.box-header -->
		  <!-- form start -->
		{{ Form::model($item, ['route' => [$module_url->update, $item->id], 'method' => 'PUT', 'files' => true]) }}
			<div class="box-body">
			  @include($form)
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary pull-right btn-simpan" dusk="btn-simpan">Simpan</button>
			</div>
			<!-- /.box-footer -->
		{{ Form::close() }}
		</div>

@endsection

@section('js')
@endsection
