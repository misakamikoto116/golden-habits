@extends('layouts.app')

@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Dashboard</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i>
      </button>
    </div>
  </div>
  
  <div class="box-body">
    <div class="col-md-12">
      <p class="text-center"><strong>User Terdaftar</strong></p>

      @php($indexWarna = 0)
      @foreach ($institution as $id => $name)
      <div class="progress-group">
        <span class="progress-text">{{ $name }}</span>
        <span class="progress-number"><b>{{ !empty($data[$id]) ? $data[$id] : 0 }}</b></span>

        <div class="progress sm">
          @if (!empty($data[$id]))
            <div class="progress-bar progress-bar-{{ $warna[$indexWarna] }}" style="width: {{ ($data[$id] / $jumlahUser) * 100 }}%"></div>
          @else 
            <div class="progress-bar" style="width: 0%"></div>
          @endif
        </div>
      </div>
      @php($indexWarna = ($indexWarna < 10) ? $indexWarna + 1 : 0)
      @endforeach

    </div>
  </div>

</div>
@endsection
